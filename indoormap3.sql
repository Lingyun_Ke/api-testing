/*
 Navicat Premium Data Transfer

 Source Server         : db
 Source Server Type    : MySQL
 Source Server Version : 50626
 Source Host           : localhost:3306
 Source Schema         : indoormap3

 Target Server Type    : MySQL
 Target Server Version : 50626
 File Encoding         : 65001

 Date: 20/08/2018 13:01:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for all_results
-- ----------------------------
DROP TABLE IF EXISTS `all_results`;
CREATE TABLE `all_results` (
  `netid` varchar(50) DEFAULT NULL,
  `courseNumber` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `platform` varchar(20) NOT NULL DEFAULT '1',
  `CountOfFirstCancelLocation` decimal(42,0) DEFAULT NULL,
  `CountOfLaterCancelLocation` decimal(42,0) DEFAULT NULL,
  `CountOfBluetoothOff` decimal(42,0) DEFAULT NULL,
  `CountOfBroadcastOFF` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButton` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButtonInClass` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButtonOutClass` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressed` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressedInClass` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressedOutClass` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressed` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressedInClass` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressedOutClass` decimal(42,0) DEFAULT NULL,
  `TapOtherUserProfile` decimal(42,0) DEFAULT NULL,
  `CountOfTapEventListTabButton` decimal(42,0) DEFAULT NULL,
  `CheckEventDetailsFromMap` decimal(42,0) DEFAULT NULL,
  `CheckEventDetailsFromList` decimal(42,0) DEFAULT NULL,
  `countOfDetectedByOtherBeacons` decimal(42,0) DEFAULT NULL,
  `autoCheckIn` bigint(22) DEFAULT NULL,
  `manuallyCheckIn` bigint(21) DEFAULT NULL,
  `firstlyInstalled` datetime DEFAULT NULL,
  `CountOfAppInitiatedByNotification` decimal(42,0) DEFAULT NULL,
  `firstTimeDetected` datetime DEFAULT NULL,
  `countOfSendEmail` bigint(21) DEFAULT NULL,
  `receiverIsStranger` bigint(21) DEFAULT NULL,
  `openAppTimes` decimal(42,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for all_results_ec1
-- ----------------------------
DROP TABLE IF EXISTS `all_results_ec1`;
CREATE TABLE `all_results_ec1` (
  `netid` varchar(50) DEFAULT NULL,
  `courseNumber` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `platform` varchar(20) NOT NULL DEFAULT '1',
  `CountOfFirstCancelLocation` decimal(42,0) DEFAULT NULL,
  `CountOfLaterCancelLocation` decimal(42,0) DEFAULT NULL,
  `CountOfBluetoothOff` decimal(42,0) DEFAULT NULL,
  `CountOfBroadcastOFF` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButton` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButtonInClass` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButtonOutClass` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressed` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressedInClass` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressedOutClass` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressed` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressedInClass` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressedOutClass` decimal(42,0) DEFAULT NULL,
  `TapOtherUserProfile` decimal(42,0) DEFAULT NULL,
  `CountOfTapEventListTabButton` decimal(42,0) DEFAULT NULL,
  `CheckEventDetailsFromMap` decimal(42,0) DEFAULT NULL,
  `CheckEventDetailsFromList` decimal(42,0) DEFAULT NULL,
  `countOfDetectedByOtherBeacons` decimal(42,0) DEFAULT NULL,
  `autoCheckIn` bigint(22) DEFAULT NULL,
  `manuallyCheckIn` bigint(21) DEFAULT NULL,
  `firstlyInstalled` datetime DEFAULT NULL,
  `CountOfAppInitiatedByNotification` decimal(42,0) DEFAULT NULL,
  `firstTimeDetected` datetime DEFAULT NULL,
  `countOfSendEmail` bigint(21) DEFAULT NULL,
  `receiverIsStranger` bigint(21) DEFAULT NULL,
  `openAppTimes` decimal(42,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for all_results_ec2
-- ----------------------------
DROP TABLE IF EXISTS `all_results_ec2`;
CREATE TABLE `all_results_ec2` (
  `netid` varchar(50) DEFAULT NULL,
  `courseNumber` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `platform` varchar(20) NOT NULL DEFAULT '1',
  `CountOfFirstCancelLocation` decimal(42,0) DEFAULT NULL,
  `CountOfLaterCancelLocation` decimal(42,0) DEFAULT NULL,
  `CountOfBluetoothOff` decimal(42,0) DEFAULT NULL,
  `CountOfBroadcastOFF` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButton` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButtonInClass` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButtonOutClass` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressed` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressedInClass` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressedOutClass` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressed` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressedInClass` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressedOutClass` decimal(42,0) DEFAULT NULL,
  `TapOtherUserProfile` decimal(42,0) DEFAULT NULL,
  `CountOfTapEventListTabButton` decimal(42,0) DEFAULT NULL,
  `CheckEventDetailsFromMap` decimal(42,0) DEFAULT NULL,
  `CheckEventDetailsFromList` decimal(42,0) DEFAULT NULL,
  `countOfDetectedByOtherBeacons` decimal(42,0) DEFAULT NULL,
  `autoCheckIn` bigint(22) DEFAULT NULL,
  `manuallyCheckIn` bigint(21) DEFAULT NULL,
  `firstlyInstalled` datetime DEFAULT NULL,
  `CountOfAppInitiatedByNotification` decimal(42,0) DEFAULT NULL,
  `firstTimeDetected` datetime DEFAULT NULL,
  `countOfSendEmail` bigint(21) DEFAULT NULL,
  `receiverIsStranger` bigint(21) DEFAULT NULL,
  `openAppTimes` decimal(42,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for all_results_mf
-- ----------------------------
DROP TABLE IF EXISTS `all_results_mf`;
CREATE TABLE `all_results_mf` (
  `netid` varchar(50) DEFAULT NULL,
  `courseNumber` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `platform` varchar(20) NOT NULL DEFAULT '1',
  `CountOfFirstCancelLocation` decimal(42,0) DEFAULT NULL,
  `CountOfLaterCancelLocation` decimal(42,0) DEFAULT NULL,
  `CountOfBluetoothOff` decimal(42,0) DEFAULT NULL,
  `CountOfBroadcastOFF` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButton` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButtonInClass` decimal(42,0) DEFAULT NULL,
  `CountOfTabSocialTabButtonOutClass` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressed` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressedInClass` decimal(42,0) DEFAULT NULL,
  `CountOfNowButtonPressedOutClass` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressed` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressedInClass` decimal(42,0) DEFAULT NULL,
  `CountOfPastButtonPressedOutClass` decimal(42,0) DEFAULT NULL,
  `TapOtherUserProfile` decimal(42,0) DEFAULT NULL,
  `CountOfTapEventListTabButton` decimal(42,0) DEFAULT NULL,
  `CheckEventDetailsFromMap` decimal(42,0) DEFAULT NULL,
  `CheckEventDetailsFromList` decimal(42,0) DEFAULT NULL,
  `countOfDetectedByOtherBeacons` decimal(42,0) DEFAULT NULL,
  `autoCheckIn` bigint(22) DEFAULT NULL,
  `manuallyCheckIn` bigint(21) DEFAULT NULL,
  `firstlyInstalled` datetime DEFAULT NULL,
  `CountOfAppInitiatedByNotification` decimal(42,0) DEFAULT NULL,
  `firstTimeDetected` datetime DEFAULT NULL,
  `countOfSendEmail` bigint(21) DEFAULT NULL,
  `receiverIsStranger` bigint(21) DEFAULT NULL,
  `openAppTimes` decimal(42,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for apikeys
-- ----------------------------
DROP TABLE IF EXISTS `apikeys`;
CREATE TABLE `apikeys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(100) NOT NULL,
  `apikey` text NOT NULL,
  `expiretime` date NOT NULL,
  `platform` varchar(20) NOT NULL DEFAULT '1',
  `email` varchar(50) DEFAULT NULL,
  `netid` varchar(50) DEFAULT NULL,
  `FirstTimeInSys` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10066 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apikeys
-- ----------------------------
BEGIN;
INSERT INTO `apikeys` VALUES (10065, 'testDeviceID', 'fadGjkjgh4676HHFnsnf', '2016-07-29', 'Android', 'test@test.com', 'tester', NULL);
COMMIT;

-- ----------------------------
-- Table structure for approvedenyevent
-- ----------------------------
DROP TABLE IF EXISTS `approvedenyevent`;
CREATE TABLE `approvedenyevent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `comment` text,
  `creatTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `decision` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for areas
-- ----------------------------
DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas` (
  `areaId` int(11) NOT NULL,
  `lattop` double DEFAULT NULL,
  `latbottom` double DEFAULT NULL,
  `longleft` double DEFAULT NULL,
  `longright` double DEFAULT NULL,
  `areaname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`areaId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for attendance
-- ----------------------------
DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(100) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32451 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attendance
-- ----------------------------
BEGIN;
INSERT INTO `attendance` VALUES (32448, 'Device ID', '2018-08-13 22:00:11');
INSERT INTO `attendance` VALUES (32449, 'Test Device ID', '2018-08-14 23:09:25');
INSERT INTO `attendance` VALUES (32450, 'Test Device ID', '2018-08-15 00:37:23');
COMMIT;

-- ----------------------------
-- Table structure for attendance_summary
-- ----------------------------
DROP TABLE IF EXISTS `attendance_summary`;
CREATE TABLE `attendance_summary` (
  `id` int(11) NOT NULL,
  `courseNumber` varchar(30) DEFAULT NULL,
  `netId` varchar(30) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for beacon
-- ----------------------------
DROP TABLE IF EXISTS `beacon`;
CREATE TABLE `beacon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(200) DEFAULT NULL,
  `major` varchar(50) DEFAULT NULL,
  `minor` varchar(50) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of beacon
-- ----------------------------
BEGIN;
INSERT INTO `beacon` VALUES (1, 'b9407f30-f5f8-466e-aff9-25556b57fe6d', '2230', '54528', 43.037621, -76.133435, 'Quad');
COMMIT;

-- ----------------------------
-- Table structure for beaconrecords
-- ----------------------------
DROP TABLE IF EXISTS `beaconrecords`;
CREATE TABLE `beaconrecords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) DEFAULT NULL,
  `major` varchar(50) DEFAULT NULL,
  `minor` varchar(50) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `device` varchar(100) DEFAULT NULL,
  `accesstime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1479 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of beaconrecords
-- ----------------------------
BEGIN;
INSERT INTO `beaconrecords` VALUES (1478, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '2230', '54528', 'user email', 'device id', 1534302564539);
COMMIT;

-- ----------------------------
-- Table structure for beaconroomrelations
-- ----------------------------
DROP TABLE IF EXISTS `beaconroomrelations`;
CREATE TABLE `beaconroomrelations` (
  `beaconNumber` varchar(100) DEFAULT NULL,
  `roomId` varchar(200) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of beaconroomrelations
-- ----------------------------
BEGIN;
INSERT INTO `beaconroomrelations` VALUES ('74278BDA-B644-4520-8F0C-720EAF059935-1-3', 'FL001', 1);
COMMIT;

-- ----------------------------
-- Table structure for beaconv2
-- ----------------------------
DROP TABLE IF EXISTS `beaconv2`;
CREATE TABLE `beaconv2` (
  `beaconName` varchar(100) DEFAULT NULL,
  `major` varchar(50) DEFAULT NULL,
  `UUID` varchar(100) DEFAULT NULL,
  `minor` varchar(50) DEFAULT NULL,
  `beaconNumber` varchar(200) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `areaId` int(11) DEFAULT '112233',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of beaconv2
-- ----------------------------
BEGIN;
INSERT INTO `beaconv2` VALUES ('MiniBeacon_00039', '1', '74278BDA-B644-4520-8F0C-720EAF059935', '3', '74278BDA-B644-4520-8F0C-720EAF059935-1-3', 3, 112233);
COMMIT;

-- ----------------------------
-- Table structure for blacklist
-- ----------------------------
DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE `blacklist` (
  `netId` varchar(50) DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for buttons
-- ----------------------------
DROP TABLE IF EXISTS `buttons`;
CREATE TABLE `buttons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `button_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for calendar
-- ----------------------------
DROP TABLE IF EXISTS `calendar`;
CREATE TABLE `calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT '0',
  `tag` varchar(100) DEFAULT NULL,
  `netid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of calendar
-- ----------------------------
BEGIN;
INSERT INTO `calendar` VALUES (238, 'event', '1532545637229-0003', 'test');
INSERT INTO `calendar` VALUES (239, 'event', 'eventid', 'test');
COMMIT;

-- ----------------------------
-- Table structure for callbacks
-- ----------------------------
DROP TABLE IF EXISTS `callbacks`;
CREATE TABLE `callbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for checkincodes
-- ----------------------------
DROP TABLE IF EXISTS `checkincodes`;
CREATE TABLE `checkincodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` varchar(50) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for chianalysis
-- ----------------------------
DROP TABLE IF EXISTS `chianalysis`;
CREATE TABLE `chianalysis` (
  `id` int(11) NOT NULL DEFAULT '0',
  `event` varchar(100) NOT NULL,
  `platform` varchar(50) DEFAULT '0',
  `device` varchar(100) DEFAULT NULL,
  `description` text,
  `data` text,
  `createTime` datetime DEFAULT NULL,
  `createTimeOfSys` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `firsttimeinsys` datetime DEFAULT NULL,
  `OnCampus` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for chiios
-- ----------------------------
DROP TABLE IF EXISTS `chiios`;
CREATE TABLE `chiios` (
  `id` int(11) NOT NULL DEFAULT '0',
  `event` varchar(100) NOT NULL,
  `platform` varchar(50) DEFAULT '0',
  `device` varchar(100) DEFAULT NULL,
  `description` text,
  `data` text,
  `createTime` datetime DEFAULT NULL,
  `createTimeOfSys` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `firsttimeinsys` datetime DEFAULT NULL,
  `OnCampus` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for courses
-- ----------------------------
DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `courseName` varchar(50) DEFAULT NULL,
  `courseID` varchar(50) DEFAULT NULL,
  `sectionNumber` varchar(20) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `startTime` varchar(20) DEFAULT NULL,
  `endTime` varchar(20) DEFAULT NULL,
  `roomId` varchar(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for coursescheduletrack
-- ----------------------------
DROP TABLE IF EXISTS `coursescheduletrack`;
CREATE TABLE `coursescheduletrack` (
  `netId` varchar(255) DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  `lastAccess` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for coursev2
-- ----------------------------
DROP TABLE IF EXISTS `coursev2`;
CREATE TABLE `coursev2` (
  `courseNumber` varchar(20) DEFAULT NULL,
  `startTime` varchar(20) DEFAULT NULL,
  `endTime` varchar(20) DEFAULT NULL,
  `instructor` varchar(50) DEFAULT NULL,
  `instructorEmail` varchar(50) DEFAULT NULL,
  `abbr` varchar(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `roomId` varchar(50) DEFAULT NULL,
  `days` varchar(20) DEFAULT NULL,
  `active` varchar(5) DEFAULT 'YES',
  `title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of coursev2
-- ----------------------------
BEGIN;
INSERT INTO `coursev2` VALUES ('testnumber', '10:00', '23:00', 'Yun Huang', 'yhuang@syr.edu', 'SALT 001', 4, 'FL001', '0,1,2,3,4,5,6', 'YES', 'SALT Daily Meeting');
COMMIT;

-- ----------------------------
-- Table structure for data_analysis_result2_v3
-- ----------------------------
DROP TABLE IF EXISTS `data_analysis_result2_v3`;
CREATE TABLE `data_analysis_result2_v3` (
  `netid` varchar(50) DEFAULT NULL,
  `platform` varchar(20) NOT NULL DEFAULT '1',
  `courseNumber` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `device` varchar(100) DEFAULT NULL,
  `firstTimeDetected` datetime DEFAULT NULL,
  `countOfSendEmail` bigint(21) DEFAULT NULL,
  `receiverIsStranger` bigint(21) DEFAULT NULL,
  `openAppTimes` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for data_analysis_result_v3
-- ----------------------------
DROP TABLE IF EXISTS `data_analysis_result_v3`;
CREATE TABLE `data_analysis_result_v3` (
  `netid` varchar(50) DEFAULT NULL,
  `platform` varchar(20) NOT NULL DEFAULT '1',
  `courseNumber` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `device` varchar(100) DEFAULT NULL,
  `CountOfFirstCancelLocation` bigint(21) DEFAULT NULL,
  `CountOfLaterCancelLocation` bigint(21) DEFAULT NULL,
  `CountOfBluetoothOff` bigint(21) DEFAULT NULL,
  `CountOfBroadcastOFF` bigint(21) DEFAULT NULL,
  `CountOfTabSocialTabButton` bigint(21) DEFAULT NULL,
  `CountOfTabSocialTabButtonInClass` bigint(21) DEFAULT NULL,
  `CountOfTabSocialTabButtonOutClass` bigint(21) DEFAULT NULL,
  `CountOfNowButtonPressed` bigint(21) DEFAULT NULL,
  `CountOfNowButtonPressedInClass` bigint(21) DEFAULT NULL,
  `CountOfNowButtonPressedOutClass` bigint(21) DEFAULT NULL,
  `CountOfPastButtonPressed` bigint(21) DEFAULT NULL,
  `CountOfPastButtonPressedInClass` bigint(21) DEFAULT NULL,
  `CountOfPastButtonPressedOutClass` bigint(21) DEFAULT NULL,
  `TapOtherUserProfile` bigint(21) DEFAULT NULL,
  `CountOfTapEventListTabButton` bigint(21) DEFAULT NULL,
  `CheckEventDetailsFromMap` bigint(21) DEFAULT NULL,
  `CheckEventDetailsFromList` bigint(21) DEFAULT NULL,
  `countOfDetectedByOtherBeacons` bigint(21) DEFAULT NULL,
  `showsup` bigint(21) DEFAULT NULL,
  `manuallyCheckIn` bigint(21) DEFAULT NULL,
  `autoCheckIn` bigint(22) DEFAULT NULL,
  `firstlyInstalled` datetime DEFAULT NULL,
  `CountOfAppInitiatedByNotification` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for email_device
-- ----------------------------
DROP TABLE IF EXISTS `email_device`;
CREATE TABLE `email_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `device` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for event_pushed_marks
-- ----------------------------
DROP TABLE IF EXISTS `event_pushed_marks`;
CREATE TABLE `event_pushed_marks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(50) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `desc` text,
  `location` varchar(200) DEFAULT NULL,
  `timebegin` varchar(50) DEFAULT NULL,
  `timeend` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `price` text,
  `sponsor` text,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `lat` double DEFAULT '0',
  `lng` double DEFAULT '0',
  `eventId` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT 'official',
  `dateinlong` bigint(20) DEFAULT NULL,
  `netid` varchar(50) DEFAULT NULL,
  `capacity` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1112407 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of events
-- ----------------------------
BEGIN;
INSERT INTO `events` VALUES (1112302, '2016-07-26', 'Yoga Foundations Class: Journey into Power', 'Take a break and join us for a free lunchtime yoga class for faculty and staff. This 30 minute yoga class is designed to teach on multiple levels (beginner to advanced) and will integrate mind and body, as we match inhale/exhale to each pose.', 'Women\'s Building, Gym A', '12:15', '12:45', 'Health', '315.443.5472', 'Free for faculty and staff', 'Wellness Initiative', 'Kim Loucy', 'kaloucy@syr.edu', 43.0351118, -76.13009269999999, '1467228290829', 'official', 1469505600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112303, '2016-07-27', 'Summer Construction Information Sessions', 'As the Division of Campus Planning, Design and Construction (CPDC) addresses more than 120 summer improvement projects, a series of information sessions will be held to update the campus community about the ongoing activity. ', 'Lyman Auditorium (Room 132)', '09:30', '23:59', 'Other', '', 'Free', 'Campus Planning, Design and Construction (CPDC)', 'Jim Blum', 'jpblum@syr.edu', 43.03849210000001, -76.1322898, '1465237543795', 'official', 1469592000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112304, '2016-07-28', 'Yoga Foundations Class: Journey into Power', 'Take a break and join us for a free lunchtime yoga class for faculty and staff. This 30 minute yoga class is designed to teach on multiple levels (beginner to advanced) and will integrate mind and body, as we match inhale/exhale to each pose.', 'Women\'s Building, Gym A', '12:15', '12:45', 'Health', '315.443.5472', 'Free for faculty and staff', 'Wellness Initiative', 'Kim Loucy', 'kaloucy@syr.edu', 43.0351118, -76.13009269999999, '1467228311871', 'official', 1469678400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112305, '2016-07-29', 'Master\'s Thesis Seminar: MS Candidate Jeremy Sloane', 'Thesis Title: The Influence of Peer-Led Team Learning on Underrepresented Minority Student Achievement in Introductory Biology and Recruitment and Retention in Science, Technology, Engineering and Mathematics Majors', 'LSC 106 Lundgren Room', '10:00', '23:59', 'Academic', '315.443.3047', 'Free', 'Biology Department', 'Katherine Geraghty', 'kygeragh@syr.edu', 43.0380994, -76.13055659999999, '1468427411970', 'official', 1469764800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112306, '2016-08-01', 'TEChack', 'TEChack is a two-day student hackathon at SyracuseCoE designed to develop IoT-enabled capabilities for products in Central New York\'s thermal and environmental controls cluster. TEChack will be led by award-winning master hackathon mentor Mihir Dani of Anaren, Inc. and will utilize Anaren\'s Atmosphere IoT Development Platform. Dani, a Syracuse University College of Engineering and Computer Science graduate, has won numerous awards representing Anaren at IOT World 2015, 2016 and Sensors Expo 2016.\n\nParking is located in the lot across the street at 718 East Washington Street, the lot surrounded by fence. Please follow the sidewalk to the front main entrance on Almond Street, across from Dunkin\' Donuts.', '727 East Washington Street, Syracuse', '08:00', '23:59', 'Academic', '315.443.8803', 'Free', 'SyracuseCoE', 'Stacy Bunce', 'sbunce@syr.edu', 43.0500718, -76.1415078, '1469113241349', 'official', 1470024000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112307, '2016-08-01', 'Assessment Help', 'Assessment Drop-In hours start Monday, June 6, 2016 at the Office of Institutional Research and Assessment (OIRA). Laura Harrington is available to answer your assessment-related questions. The Quad Shuttle stops outside of OIRA, located at 400 Ostrom Ave. Visitor parking is also available. For more information, contact Laura Harrington.\n\nAssessment-related questions can also be emailed to the Assessment Working Team at assessment@syr.edu.', 'OIRA, 400 Ostrom Ave.', '10:00', '14:00', 'Work', '315.443.1415', '', 'Office of the Associate Provost for Academic Programs', 'Laura Harrington', 'lalvut@syr.edu', 43.0423025, -76.12907190000001, '1465232529789-0004', 'official', 1470024000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112308, '2016-08-01', 'Monday Mile Community Walk', 'Get moving with the Monday Mile and earn prizes. Sign up online, get your Monday Mile passport and get going! Join others to walk, jog or stroll the Monday Mile.  \n\nThe Monday Mile is a fun way to achieve your fitness goals by getting out to move a mile for your health. There are many Monday Mile walking routes across Onondaga County.  \n\nEveryone is welcome to come enjoy this non-competitive fitness initiative.', 'Barry Park, meet at the intersection of Meadowbrook Drive and Broad Street near the playground', '17:15', '17:45', 'Health', '315.443.9343', 'Free', 'Healthy Monday Syracuse', 'Leah Moser', 'healthymonday@syr.edu', 43.0291204, -76.1171363, '1463766710125', 'official', 1470024000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112309, '2016-08-02', 'TEChack', 'TEChack is a two-day student hackathon at SyracuseCoE designed to develop IoT-enabled capabilities for products in Central New York\'s thermal and environmental controls cluster. TEChack will be led by award-winning master hackathon mentor Mihir Dani of Anaren, Inc. and will utilize Anaren\'s Atmosphere IoT Development Platform. Dani, a Syracuse University College of Engineering and Computer Science graduate, has won numerous awards representing Anaren at IOT World 2015, 2016 and Sensors Expo 2016.\n\nParking is located in the lot across the street at 718 East Washington Street, the lot surrounded by fence. Please follow the sidewalk to the front main entrance on Almond Street, across from Dunkin\' Donuts.', '727 East Washington Street, Syracuse', '11:30', '23:59', 'Academic', '315.443.8803', 'Free', 'SyracuseCoE', 'Stacy Bunce', 'sbunce@syr.edu', 43.0500718, -76.1415078, '1469114736861', 'official', 1470110400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112310, '2016-08-02', 'AM-TEC Innovation Showcase', 'The AM-TEC Innovation Showcase will exhibit student-produced project posters and feature an interactive demonstration highlighting the many potential applications of the Internet of Things (IOT) for AM-TEC products. Information about members of the AM-TEC Partner Program will be available. \nDinosaur Bar-B-Que treats and drinks will be provided.\n \nParking is located in the lot across the street at 718 East Washington Street, the lot surrounded by fence. Please follow the sidewalk to the front main entrance on Almond Street, across from Dunkin\' Donuts. ', 'SyracuseCoE Headquarters, 727 East Washington Street, 2nd Floor, Syracuse', '15:30', '23:59', 'Work', '315.443.8803', 'Free', 'SyracuseCoE', 'Stacy Bunce', 'sbunce@syr.edu', 43.0500718, -76.1415078, '1469822297902', 'official', 1470110400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112311, '2016-08-05', 'Viewing Event for 2016 Rio Olympics Opening Ceremony', 'Watch the colorful pageantry and catch all your favorite international athletes--and your favorite Orange competitors--at a campus viewing party of the Opening Ceremony for the 2016 Rio Olympics. \n\nThe campus community is invited to watch the Opening Ceremony for the 2016 Rio Olympics at the Slutzker Center for International Services, 310 Walnut Place. Attendees can learn about Brazilian culture, eat Brazilian food, talk sports and maybe even Samba.\n', 'Slutzker Center for International Services, 310 Walnut Place, Syracuse', '18:30', '23:59', 'Work', '', '', 'Slutzker Center for International Services', '', 'lescis@syr.edu', 43.0407207, -76.1331505, '1470347334583', 'official', 1470369600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112312, '2016-08-08', 'Assessment Help', 'Assessment Drop-In hours start Monday, June 6, 2016 at the Office of Institutional Research and Assessment (OIRA). Laura Harrington is available to answer your assessment-related questions. The Quad Shuttle stops outside of OIRA, located at 400 Ostrom Ave. Visitor parking is also available. For more information, contact Laura Harrington.\n\nAssessment-related questions can also be emailed to the Assessment Working Team at assessment@syr.edu.', 'OIRA, 400 Ostrom Ave.', '10:00', '14:00', 'Work', '315.443.1415', '', 'Office of the Associate Provost for Academic Programs', 'Laura Harrington', 'lalvut@syr.edu', 43.0423025, -76.12907190000001, '1465232529789-0005', 'official', 1470628800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112313, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, '08015009-3ca0-441d-b60e-fc55cc946957', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112314, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, '4658c5d7-1edc-4286-a29d-e9332e817c81', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112315, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, '272bc421-6b1e-4eaa-8efb-ccf49baeb399', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112316, '2018-08-14', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530541975692', 'official', 1534219200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112317, '2018-08-14', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0004', 'official', 1534219200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112318, '2018-08-14', 'Stretch and Breathe', 'This free 20-minute class will clear your mind and energize you for the afternoon ahead. All levels of experience and ability are welcome. ', 'Various locations', '12:10', '12:30', 'Health', '', 'Free for faculty and staff', '', 'Wellness Initiative', 'wellness@syr.edu', 43.0481221, -76.14742439999999, '1527078511216-0011', 'official', 1534219200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112319, '2018-08-15', 'Historical Tour of Syracuse University Campus', 'Join Professor Emeritus Marvin Druger for an extremely informative tour of the Syracuse University campus as he highlights special buildings and spaces on campus and provides unique aspects of Syracuse University history.', 'Place of Remembrance', '12:00', '13:30', 'Other', '', '', '', '', '', 43.039702, -76.0821836, '1527077772333', 'official', 1534305600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112320, '2018-08-16', 'Biology Dissertation Defense Seminar - Elise Hinman', 'SPEAKER: Elise Hinman\n\nTITLE: Survival in the Forest Understory: Resilience and Resistance to Damage in Native and Invasive Woody Plants\n\nADVISOR: Jason Fridley ', '106 Life Sciences Complex', '10:00', '23:59', 'Academic', '315.443.9154', '', 'Biology', 'Lynn Fall', 'lfall@syr.edu', 43.0380482, -76.130552, '1527190101491', 'official', 1534392000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112321, '2018-08-16', 'Stretch and Breathe', 'This free 20-minute class will clear your mind and energize you for the afternoon ahead. All levels of experience and ability are welcome. ', 'Various locations', '12:10', '12:30', 'Health', '', 'Free for faculty and staff', '', 'Wellness Initiative', 'wellness@syr.edu', 43.0481221, -76.14742439999999, '1527078592923-0011', 'official', 1534392000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112322, '2018-08-17', 'Biology Dissertation Defense Seminar: Fatmagul Bahar', 'SPEAKER: Fatmagul Bahar\nTITLE: Developing a New Mechanical Model for Swarm Development in Myxococcus xanthus, and Establishing a Genotype-to-Phenotype Correlation Between Swarm Pattern Formation and Gene Homology\nADVISOR: Roy Welch ', '106 Life Sciences Complex', '09:30', '23:59', 'Academic', '315.443.9154', '', 'Biology', 'Lynn Fall', 'lfall@syr.edu', 43.0380482, -76.130552, '1532101682115', 'official', 1534478400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112323, '2018-08-17', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530542100577', 'official', 1534478400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112324, '2018-08-21', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530541995287', 'official', 1534824000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112325, '2018-08-21', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0005', 'official', 1534824000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112326, '2018-08-24', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530542115916', 'official', 1535083200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112327, '2018-08-26', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532109067383', 'official', 1535256000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112328, '2018-08-28', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530542007917', 'official', 1535428800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112329, '2018-08-28', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0006', 'official', 1535428800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112330, '2018-08-31', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530542130482', 'official', 1535688000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112331, '2018-09-02', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110820790', 'official', 1535860800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112332, '2018-09-04', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530542020977', 'official', 1536033600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112333, '2018-09-04', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0007', 'official', 1536033600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112334, '2018-09-07', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530542143265', 'official', 1536292800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112335, '2018-09-09', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110530001-0001', 'official', 1536465600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112336, '2018-09-11', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0002', 'official', 1536638400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112337, '2018-09-11', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0008', 'official', 1536638400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112338, '2018-09-14', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530542287340', 'official', 1536897600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112339, '2018-09-16', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110530001-0002', 'official', 1537070400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112340, '2018-09-18', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0003', 'official', 1537243200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112341, '2018-09-18', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0009', 'official', 1537243200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112342, '2018-09-20', 'GYT Free STI Testing Event', 'GYT--Get Yourself Tested! Free STI (STD) testing event for Syracuse University students. From 9am to 4pm. Enter through the Health Promotion entrance at 111 Waverly Ave. and bring your Syracuse University ID.', '111 Waverly Ave., Syracuse', '09:00', '16:00', 'Health', '315.443.4127', 'Free', 'Health Services, Student Association and Office of Health Promotion', 'LeeAnne Lane, RN', 'lalane@syr.edu', 43.0391573, -76.1373098, '1532114548573-0001', 'official', 1537416000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112343, '2018-09-21', 'GYT Free STI Testing Event', 'GYT--Get Yourself Tested! Free STI (STD) testing event for Syracuse University students. From 9am to 4pm. Enter through the Health Promotion entrance at 111 Waverly Ave. and bring your Syracuse University ID.', '111 Waverly Ave., Syracuse', '09:00', '16:00', 'Health', '315.443.4127', 'Free', 'Health Services, Student Association and Office of Health Promotion', 'LeeAnne Lane, RN', 'lalane@syr.edu', 43.0391573, -76.1373098, '1532114548573-0002', 'official', 1537502400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112344, '2018-09-21', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530542301192', 'official', 1537502400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112345, '2018-09-22', '7th International Building Physics Conference', 'The International Building Physics Conference (IBPC) is the official international conference series of the International Association of Building Physics. IBPC will be held in the United States for the first time, on September 23-26, 2018, in Syracuse, New York, hosted by Syracuse University and the Syracuse Center of Excellence in Environmental and Energy Systems (SyracuseCoE). \n\nThe theme of IBPC2018 is Healthy, Intelligent and Resilient Buildings and Urban Environments. It will provide a forum for scientific, technological and design exchanges through multiple platforms: \n* Presentations of original research and development work and findings \n* Demonstrations and exhibitions of innovative green building technologies \n* Discussions of future challenges and opportunities \n\nIBPC2018 will kick-off on Sunday, Sept. 23, 2018 with a welcome reception at the Marriott Syracuse Downtown. The technical program will include keynote presentations in plenary sessions, parallel sessions and poster sessions. Evening events include a reception and student poster competition at SyracuseCoE headquarters, an iconic LEED Platinum research testbed, on Monday, and the conference dinner and entertainment. Closing ceremonies will be held at noon on Wednesday, Sept. 26. There will also be post conference events.', '100 E. Onondaga St., Syracuse', '08:00', '23:59', 'Academic', '', '', 'SyracuseCoE', 'Kerrie Marshall', 'klmarsha@syr.edu', 43.0391573, -76.1373098, '1497639426914', 'official', 1537588800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112346, '2018-09-23', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110530001-0003', 'official', 1537675200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112347, '2018-09-25', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0004', 'official', 1537848000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112348, '2018-09-25', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0010', 'official', 1537848000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112349, '2018-09-28', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548088279', 'official', 1538107200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112350, '2018-09-30', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110530001-0004', 'official', 1538280000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112351, '2018-10-02', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0005', 'official', 1538452800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112352, '2018-10-02', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0011', 'official', 1538452800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112353, '2018-10-05', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530542318867', 'official', 1538712000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112354, '2018-10-07', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110557384-0001', 'official', 1538884800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112355, '2018-10-09', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0006', 'official', 1539057600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112356, '2018-10-09', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0012', 'official', 1539057600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112357, '2018-10-12', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0001', 'official', 1539316800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112358, '2018-10-14', 'Eighth Annual It Girls Overnight Retreat', 'The Eighth Annual It Girls Overnight Retreat presented by the School of Information Studies (iSchool) is an event to build confidence, inspire and create a pathway for high school girls to study information technology in college. An essential piece of the program involves creating meaningful connections between professional women, iSchool faculty, staff, college students and alumni. Most important is to build lasting friendships and interest in technology. ', 'Hinds Hall, Syracuse University', '12:00', '23:59', 'Other', '315.443.3439', '$25', 'Syracuse University School of Information Studies', 'Stephanie Worden', 'ssworden@syr.edu', 43.038222, -76.13336799999999, '1532113446391', 'official', 1539489600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112359, '2018-10-14', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110557384-0002', 'official', 1539489600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112360, '2018-10-16', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0007', 'official', 1539662400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112361, '2018-10-16', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0013', 'official', 1539662400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112362, '2018-10-18', 'When Running Made History: A Book Talk and Signing with Roger Robinson', 'Attend a talk and book signing by Roger Robinson, author of \"When Running Made History,\" a vivid eyewitness account of running events that influenced the world. His book was published by Syracuse University Press in 2018. Robinson is a world-class runner, journalist, editor and author of numerous books. He splits time between New York and New Zealand with his wife, women\'s running activist Kathrine Switzer \'68, G\'72, Syracuse University\'s 2018 Commencement speaker.\n', '114 Bird Library, Peter Graham Scholarly Commons', '17:00', '23:59', 'Academic', '315.443.5547', '', 'Syracuse University Press', 'Mona Hamlin', 'mhamlin@syr.edu', 43.0391573, -76.1373098, '1532114278247', 'official', 1539835200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112363, '2018-10-19', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0002', 'official', 1539921600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112364, '2018-10-21', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110557384-0003', 'official', 1540094400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112365, '2018-10-23', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0008', 'official', 1540267200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112366, '2018-10-23', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0014', 'official', 1540267200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112367, '2018-10-26', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0003', 'official', 1540526400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112368, '2018-10-28', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110557384-0004', 'official', 1540699200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112369, '2018-10-30', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0009', 'official', 1540872000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112370, '2018-10-30', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library. \n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532357265756-0015', 'official', 1540872000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112371, '2018-11-02', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0004', 'official', 1541131200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112372, '2018-11-04', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110577159-0001', 'official', 1541304000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112373, '2018-11-06', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0010', 'official', 1541480400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112374, '2018-11-06', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library.\n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532545637229-0001', 'official', 1541480400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112375, '2018-11-09', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0005', 'official', 1541739600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112376, '2018-11-11', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110577159-0002', 'official', 1541912400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112377, '2018-11-13', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0011', 'official', 1542085200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112378, '2018-11-13', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library.\n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532545637229-0002', 'official', 1542085200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112379, '2018-11-16', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0006', 'official', 1542344400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112380, '2018-11-18', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110577159-0003', 'official', 1542517200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112381, '2018-11-20', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0012', 'official', 1542690000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112382, '2018-11-20', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library.\n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532545637229-0003', 'official', 1542690000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112383, '2018-11-25', 'Dean\'s Convocation', 'All are invited for the Dean\'s Convocation, a weekly gathering on Sundays in Hendricks Chapel, intended to be a place for all people, featuring music and reflection from a diversity of religious, spiritual and philosophical perspectives. Each week will be a new theme.', 'Hendricks Main Chapel', '19:00', '20:00', 'Other', '315.443.2903', 'Free', '', 'Michelle Larrabee', 'melarrab@syr.edu', 43.0391534, -76.1351158, '1532110577159-0004', 'official', 1543122000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112384, '2018-11-27', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0013', 'official', 1543294800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112385, '2018-11-27', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library.\n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532545637229-0004', 'official', 1543294800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112386, '2018-11-30', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0008', 'official', 1543554000000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112387, '2018-12-04', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0014', 'official', 1543899600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112388, '2018-12-04', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library.\n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532545637229-0005', 'official', 1543899600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112389, '2018-12-07', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0009', 'official', 1544158800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112390, '2018-12-11', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0015', 'official', 1544504400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112391, '2018-12-11', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library.\n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532545637229-0006', 'official', 1544504400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112392, '2018-12-14', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n  \n', 'Skytop Office Building, Office of Human Resources, Conference Room D', '13:00', '15:00', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0106, -76.1159447, '1530548268458-0010', 'official', 1544763600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112393, '2018-12-18', 'Excellus BCBS Office Hours', 'Meet one-on-one with an experienced Excellus BCBS representative for personalized assistance regarding your health plan questions. Registration is not required.\n\n', 'Steele Hall, Room 210', '08:30', '10:30', 'Other', '315.443.4042', '', '', 'HR Service Center', 'hrservice@syr.edu', 43.0373718, -76.1355275, '1530548204769-0016', 'official', 1545109200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112394, '2018-12-18', 'Orange Orators Toastmasters Club Meeting', 'In the Orange Orators, you\'ll learn to deliver great presentations, easily lead teams and conduct meetings, give and receive constructive evaluations, and become a better listener. We meet every Tuesday from noon to 1 p.m. in the Hillyer Room on the sixth floor Syracuse University Bird Library.\n\nMembers develop communication and leadership skills, which foster self-confidence and personal growth, all in a mutually supportive environment. Membership is open to SU students, staff, faculty, and the Central New York community. Visitors are always welcome! ', 'Hillyer Room, 606 Bird Library', '12:00', '13:00', 'Work', '315.443.3536', '', '', 'Ron Bunal', 'rmbunal@syr.edu', 43.0396856, -76.1326069, '1532545637229-0007', 'official', 1545109200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112395, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, '8aaf5445-785a-4f06-89c9-18a8b9bedd4e', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112396, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, '54e96faf-80fa-42f2-9959-88a53a24ca35', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112397, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, 'b7b2f729-8e1e-43c7-8c8f-26b637c5d7f8', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112398, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, 'c519d012-47c4-4b9a-93c5-9d8f52445bc7', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112399, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, '605eb843-b0d2-45cf-bd0c-2764ef747b4d', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112400, '2018-08-20', 'English Language Assessment Exam', 'Required for all non-native English speakers.', 'Gifford Auditorium, H.B. Crouse Hall', '08:30', '23:59', 'Academic', '', '', 'Office of Academic Affairs', '', '', 43.0381892, -76.134215, '1534283511323', 'official', 1534737600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112401, '2018-08-23', 'New Student Convocation', 'Chancellor and President Kent Syverud and Vice Chancellor and Provost Michele Wheatly welcome the incoming class of new students to Syracuse University.', 'Carrier Dome', '17:30', '23:59', 'Work', '', '', 'Office of Academic Affairs', 'Office of Academic Affairs', '', 43.0362269, -76.1363161, '1534276241610', 'official', 1534996800000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112402, '2018-08-24', 'iSchool Undergraduate Welcome Family Breakfast', 'Welcome families of new undergraduate students to the iSchool. Brief discussion about the program.', 'Tent Outside Hinds Hall', '09:00', '23:59', 'Work', '315.443.2736', '', 'School of Information Studies', 'Mariann Major', 'mmajor01@syr.edu', 43.0391573, -76.1373098, '1534276398795', 'official', 1535083200000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112403, '2018-08-25', 'Orange After Dark: Orange Palooza', 'Join your fellow new students for the first Orange After Dark late night event of the year! Enjoy a fun-filled night on the Women\'s Building field with free food, inclusive games and other entertainment! Pick up your new OAD schedule so you don\'t miss any events this semester! \n\nRain Location: Flanagan Gym\n\nValid Syracuse University ID required to attend.', 'Women\'s Building Field', '20:30', '00:00', 'Social', '315.443.2718', 'Free, no ticket needed', 'Orange After Dark, Office of Student Activities', 'Courtney Jones', 'cejone0@syr.edu', 43.0351118, -76.13009269999999, '1534194651614', 'official', 1535169600000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112404, '2018-08-27', 'First Day of Classes', 'Instruction begins.', '', '07:00', '23:59', 'Other', '', '', 'Office of the Registrar', '', '', 43.0481221, -76.14742439999999, '1534283206756', 'official', 1535342400000, NULL, NULL, 1);
INSERT INTO `events` VALUES (1112405, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, '8402a647-df4d-4799-aa39-7260db7c7948', 'userevent', 1533873600000, 'test', '20', 0);
INSERT INTO `events` VALUES (1112406, '2018-08-10', 'event title', 'event desc', 'event\'s location', '3:00', '4:00', 'Academic', '301-332-4221', NULL, NULL, NULL, 'test@test.com', 37.232323, -122.0434, '065b4348-e929-4e0d-987c-45fd1c9f5aa2', 'userevent', 1533873600000, 'test', '20', 0);
COMMIT;

-- ----------------------------
-- Table structure for events_copy
-- ----------------------------
DROP TABLE IF EXISTS `events_copy`;
CREATE TABLE `events_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(50) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `desc` text,
  `location` varchar(200) DEFAULT NULL,
  `timebegin` varchar(50) DEFAULT NULL,
  `timeend` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `price` text,
  `sponsor` text,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `lat` double DEFAULT '0',
  `lng` double DEFAULT '0',
  `eventId` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT 'official',
  `dateinlong` bigint(20) DEFAULT NULL,
  `netid` varchar(50) DEFAULT NULL,
  `capacity` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for firebasetokens
-- ----------------------------
DROP TABLE IF EXISTS `firebasetokens`;
CREATE TABLE `firebasetokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(100) NOT NULL,
  `token` text NOT NULL,
  `platform` varchar(20) NOT NULL DEFAULT '1',
  `email` varchar(50) DEFAULT NULL,
  `netid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for leaderboard
-- ----------------------------
DROP TABLE IF EXISTS `leaderboard`;
CREATE TABLE `leaderboard` (
  `roomId` varchar(200) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for locations
-- ----------------------------
DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(200) DEFAULT NULL,
  `lat` double DEFAULT '0',
  `lng` double DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `floor` varchar(50) DEFAULT '0',
  `roomNo` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT '0',
  `alt` double DEFAULT '0',
  `locationType` varchar(10) DEFAULT '',
  `alias` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of locations
-- ----------------------------
BEGIN;
INSERT INTO `locations` VALUES (1, 'Syracuse, NY 13210, USA', 43.0373718, -76.1355275, 2, NULL, NULL, NULL, 0, NULL, 'Steele Hall, Room 210');
INSERT INTO `locations` VALUES (2, 'Hillyer Room, Syracuse, NY 13210, USA', 43.0396856, -76.1326069, 2, NULL, NULL, NULL, 0, NULL, 'Hillyer Room, 606 Bird Library');
INSERT INTO `locations` VALUES (3, 'Syracuse, NY, USA', 43.0481221, -76.14742439999999, 2, NULL, NULL, NULL, 0, NULL, 'Various locations');
INSERT INTO `locations` VALUES (4, 'Life Sciences Complex, 107 College Pl, Syracuse, NY 13210, USA', 43.0380482, -76.130552, 2, NULL, NULL, NULL, 0, NULL, '106 Life Sciences Complex');
INSERT INTO `locations` VALUES (5, '101 Skytop Rd, Syracuse, NY 13244, USA', 43.0106, -76.1159447, 2, NULL, NULL, NULL, 0, NULL, 'Skytop Office Building, Office of Human Resources, Conference Room D');
INSERT INTO `locations` VALUES (6, 'Syracuse University Hall of Languages, University Pl, Syracuse, NY 13210, USA', 43.0391534, -76.1351158, 2, NULL, NULL, NULL, 0, NULL, 'Hendricks Main Chapel');
INSERT INTO `locations` VALUES (7, 'Hillyer Room, 606 Bird Library', 43.0391573, -76.1373098, 0, NULL, NULL, NULL, 0, NULL, 'Hillyer Room, 606 Bird Library');
INSERT INTO `locations` VALUES (8, 'Steele Hall, Room 210', 43.0391573, -76.1373098, 0, NULL, NULL, NULL, 0, NULL, 'Steele Hall, Room 210');
INSERT INTO `locations` VALUES (9, '111 Waverly Ave., Syracuse', 43.0391573, -76.1373098, 0, NULL, NULL, NULL, 0, NULL, '111 Waverly Ave., Syracuse');
INSERT INTO `locations` VALUES (10, 'Skytop Office Building, Office of Human Resources, Conference Room D', 43.0391573, -76.1373098, 0, NULL, NULL, NULL, 0, NULL, 'Skytop Office Building, Office of Human Resources, Conference Room D');
INSERT INTO `locations` VALUES (11, '100 E. Onondaga St., Syracuse', 43.0391573, -76.1373098, 0, NULL, NULL, NULL, 0, NULL, '100 E. Onondaga St., Syracuse');
INSERT INTO `locations` VALUES (12, 'Hendricks Main Chapel', 43.0391573, -76.1373098, 0, NULL, NULL, NULL, 0, NULL, 'Hendricks Main Chapel');
INSERT INTO `locations` VALUES (13, 'Hinds Hall, 343 Hinds Hall, Syracuse, NY 13210, USA', 43.038222, -76.13336799999999, 2, NULL, NULL, NULL, 0, NULL, 'Hinds Hall, Syracuse University');
INSERT INTO `locations` VALUES (14, '2810, 820 Comstock Ave, Syracuse, NY 13210, United States', 43.0351118, -76.13009269999999, 2, NULL, NULL, NULL, 0, NULL, 'Women\'s Building, Gym A');
INSERT INTO `locations` VALUES (15, 'Life Sciences Complex, Syracuse, NY 13210, USA', 43.0380994, -76.13055659999999, 2, NULL, NULL, NULL, 0, NULL, 'LSC 106 Lundgren Room');
INSERT INTO `locations` VALUES (16, '727 E Washington St, Syracuse, NY 13210, USA', 43.0500718, -76.1415078, 2, NULL, NULL, NULL, 0, NULL, '727 East Washington Street, Syracuse');
INSERT INTO `locations` VALUES (17, 'Ostrom, 400 Ostrom Ave, Syracuse, NY 13210, USA', 43.0423025, -76.12907190000001, 2, NULL, NULL, NULL, 0, NULL, 'OIRA, 400 Ostrom Ave.');
INSERT INTO `locations` VALUES (18, 'Barry Park, Meadowbrook Dr, Syracuse, NY 13210, USA', 43.0291204, -76.1171363, 2, NULL, NULL, NULL, 0, NULL, 'Barry Park, meet at the intersection of Meadowbrook Drive and Broad Street near the playground');
INSERT INTO `locations` VALUES (19, '727 E Washington St, Syracuse, NY 13244, USA', 43.0500718, -76.1415078, 2, NULL, NULL, NULL, 0, NULL, 'SyracuseCoE Headquarters, 727 East Washington Street, 2nd Floor, Syracuse');
INSERT INTO `locations` VALUES (20, '2410, 310, Walnut Pl, Syracuse, NY 13210, United States', 43.0407207, -76.1331505, 2, NULL, NULL, NULL, 0, NULL, 'Slutzker Center for International Services, 310 Walnut Place, Syracuse');
INSERT INTO `locations` VALUES (21, '114 Bird Library, Peter Graham Scholarly Commons', 43.0391573, -76.1373098, 0, NULL, NULL, NULL, 0, NULL, '114 Bird Library, Peter Graham Scholarly Commons');
INSERT INTO `locations` VALUES (22, '3800 E Genesee St, Syracuse, NY 13214, USA', 43.039702, -76.0821836, 2, NULL, NULL, NULL, 0, NULL, 'Place of Remembrance');
INSERT INTO `locations` VALUES (23, 'Gifford Auditorium, Syracuse, NY 13210, USA', 43.0381892, -76.134215, 2, NULL, NULL, NULL, 0, NULL, 'Gifford Auditorium, H.B. Crouse Hall');
INSERT INTO `locations` VALUES (24, '900 Irving Ave, Syracuse, NY 13244, USA', 43.0362269, -76.1363161, 2, NULL, NULL, NULL, 0, NULL, 'Carrier Dome');
INSERT INTO `locations` VALUES (25, 'Tent Outside Hinds Hall', 43.0391573, -76.1373098, 0, NULL, NULL, NULL, 0, NULL, 'Tent Outside Hinds Hall');
COMMIT;

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `button_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parameters` text,
  `url` text,
  `_date` varchar(200) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `device_id` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `content` text,
  `sender` varchar(50) DEFAULT NULL,
  `receiver` varchar(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for photorecords
-- ----------------------------
DROP TABLE IF EXISTS `photorecords`;
CREATE TABLE `photorecords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) DEFAULT NULL,
  `major` varchar(50) DEFAULT NULL,
  `minor` varchar(50) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `device` varchar(100) DEFAULT NULL,
  `accesstime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for photos
-- ----------------------------
DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `lat` double DEFAULT '0',
  `lng` double DEFAULT '0',
  `suffix` varchar(255) DEFAULT 'jpg',
  `location` varchar(255) DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  `alt` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of photos
-- ----------------------------
BEGIN;
INSERT INTO `photos` VALUES (216, '15a3f5fa-0d16-4688-a2a5-c36dd35fd8f6', '2016-06-21 23:28:04', 43.03910583073029, -76.13730411976576, 'jpg', '104 University Pl Syracuse, NY 13210 ', 1, 0);
INSERT INTO `photos` VALUES (217, 'e489cb87-a78a-41fa-bb8b-bf07c9c5d5e8', '2016-06-21 23:28:04', 43.03910583073029, -76.13730411976576, 'jpg', '104 University Pl Syracuse, NY 13210 ', 2, 0);
INSERT INTO `photos` VALUES (218, 'f317aab0-05e0-42ea-a02c-62c1087010ef', '2016-06-21 23:28:04', 43.03910583073029, -76.13730411976576, 'jpg', '104 University Pl Syracuse, NY 13210 ', 3, 0);
INSERT INTO `photos` VALUES (219, '0ed9f094-1d5a-4c42-b96f-89602a93dd50', '2016-06-21 23:28:04', 43.03910583073029, -76.13730411976576, 'jpg', '104 University Pl Syracuse, NY 13210 ', 4, 0);
INSERT INTO `photos` VALUES (220, '6904cdde-69c8-493d-9639-7e8239aaf4b1', '2016-06-21 23:28:04', 43.03910583073029, -76.13730411976576, 'jpg', '104 University Pl Syracuse, NY 13210 ', 5, 0);
INSERT INTO `photos` VALUES (221, '9a11948b-0da1-4759-9634-b97a88a3111c', '2016-07-01 15:21:10', 43.04052661020643, -76.13459374755621, 'jpg', '101-199 Waverly Ave Syracuse, NY 13210 ', 6, 0);
INSERT INTO `photos` VALUES (222, '3ef448f7-e5fa-43f6-bf8b-41fd8e59ff0b', '2016-07-01 15:21:10', 43.04052661020643, -76.13459374755621, 'jpg', '101-199 Waverly Ave Syracuse, NY 13210 ', 7, 0);
INSERT INTO `photos` VALUES (223, '425225da-2ead-4574-8b10-43a39c921232', '2016-07-01 15:46:00', 43.03837680468849, -76.13942809402943, 'jpg', '800 Irving Ave Syracuse, NY 13210 ', 8, 0);
INSERT INTO `photos` VALUES (224, 'ab92374e-0d2c-4469-9e9d-4aa8c777b21b', '2016-07-01 15:46:00', 43.03837680468849, -76.13942809402943, 'jpg', '800 Irving Ave Syracuse, NY 13210 ', 9, 0);
INSERT INTO `photos` VALUES (225, '0d8678e3-a2b2-4f9d-9cdd-07732a865822', '2016-07-01 15:51:26', 43.03910583073029, -76.13730411976576, 'jpg', '104 University Pl Syracuse, NY 13210 ', 10, 0);
INSERT INTO `photos` VALUES (226, '316e99e0-f134-420d-97c3-c281fc0761cf', '2016-07-01 15:51:26', 43.03910583073029, -76.13730411976576, 'jpg', '104 University Pl Syracuse, NY 13210 ', 11, 0);
COMMIT;

-- ----------------------------
-- Table structure for photosv2
-- ----------------------------
DROP TABLE IF EXISTS `photosv2`;
CREATE TABLE `photosv2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photoname` varchar(200) NOT NULL,
  `feeling` text NOT NULL,
  `place` varchar(300) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  `netid` varchar(50) NOT NULL DEFAULT 'ysun102',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of photosv2
-- ----------------------------
BEGIN;
INSERT INTO `photosv2` VALUES (1, 'b4f84150-b4f8-4f66-a490-c26050b34caa', 'Would throw this guy out!', 'Syracuse University, Syracuse, NY, 13244', 43.038248, -76.133516, '2016-12-02 14:02:58', 'Who ate my snack?!', 'jliu194');
INSERT INTO `photosv2` VALUES (2, 'b75939c6-3eda-4916-bb5a-462a2fd00e0f', 'Great!', '32 Julia Cir, Setauket- East Setauket, NY 11733, USA', 43.038246, -76.133489, '2016-12-02 15:26:41', 'User hasn\'t type in anything', 'qwu114');
INSERT INTO `photosv2` VALUES (3, 'ca7962eb-1e10-4202-9429-9f6661cacbf4', 'Boring', 'Syracuse University, Syracuse, NY, 13244', 43.038251, -76.133558, '2016-12-02 15:30:04', 'No new thing to write...', 'lke101');
INSERT INTO `photosv2` VALUES (4, '356cbe90-5360-415e-9550-d77c99ae86c4', 'Great', 'Syracuse, NY 13210, USA', 43.0373718, -76.1355275, '2018-08-14 22:49:53', 'test description', 'test');
INSERT INTO `photosv2` VALUES (5, '30ce2509-99fe-4048-bf72-2690dc19d110', 'Great', 'Syracuse, NY 13210, USA', 43.0373718, -76.1355275, '2018-08-14 23:09:25', 'test description', 'test');
INSERT INTO `photosv2` VALUES (6, 'e4d3a7f4-563c-47c4-b898-eb30a415d5d8', 'Great', 'Syracuse, NY 13210, USA', 43.0373718, -76.1355275, '2018-08-15 00:37:24', 'test description', 'test');
COMMIT;

-- ----------------------------
-- Table structure for records
-- ----------------------------
DROP TABLE IF EXISTS `records`;
CREATE TABLE `records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(100) NOT NULL,
  `platform` varchar(50) DEFAULT '0',
  `device` varchar(100) DEFAULT NULL,
  `description` text,
  `data` text,
  `createTime` datetime DEFAULT NULL,
  `createTimeOfSys` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=409204 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of records
-- ----------------------------
BEGIN;
INSERT INTO `records` VALUES (409201, 'event name', 'IOS', 'Device ID', '', 'test', '2016-08-13 16:16:57', '2018-08-13 22:00:11', 40.9383469, -73.0862273);
INSERT INTO `records` VALUES (409202, 'event name', 'IOS', 'Test Device ID', '', 'test', '2016-08-13 16:16:57', '2018-08-14 23:09:25', 40.9383469, -73.0862273);
INSERT INTO `records` VALUES (409203, 'event name', 'IOS', 'Test Device ID', '', 'test', '2016-08-13 16:16:57', '2018-08-15 00:37:23', 40.9383469, -73.0862273);
COMMIT;

-- ----------------------------
-- Table structure for roomcheckin
-- ----------------------------
DROP TABLE IF EXISTS `roomcheckin`;
CREATE TABLE `roomcheckin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `netId` varchar(50) DEFAULT NULL,
  `roomId` varchar(50) DEFAULT NULL,
  `enterTime` timestamp NULL DEFAULT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `altit` double DEFAULT '0',
  `locationSource` varchar(10) DEFAULT 'GPS',
  `by` varchar(10) DEFAULT 'STU',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roomcheckin
-- ----------------------------
BEGIN;
INSERT INTO `roomcheckin` VALUES (1, NULL, 'FL001', '1970-01-18 13:11:40', 1800000, 0, 43.0373718, -76.1355275, 0, 'TEST', 'STU');
INSERT INTO `roomcheckin` VALUES (2, NULL, 'FL001', '1970-01-18 13:11:40', 1800000, 0, 43.0373718, -76.1355275, 0, 'TEST', 'STU');
INSERT INTO `roomcheckin` VALUES (3, NULL, 'FL001', '1970-01-18 13:11:40', 1800000, 0, 43.0373718, -76.1355275, 0, 'TEST', 'STU');
INSERT INTO `roomcheckin` VALUES (4, NULL, 'FL001', '1970-01-18 13:11:40', 1800000, 0, 43.0373718, -76.1355275, 0, 'TEST', 'STU');
COMMIT;

-- ----------------------------
-- Table structure for roomlogs
-- ----------------------------
DROP TABLE IF EXISTS `roomlogs`;
CREATE TABLE `roomlogs` (
  `roomId` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `netId` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `enterTime` datetime DEFAULT NULL,
  `exitTime` datetime DEFAULT NULL,
  `roomlogId` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `hidden` int(11) DEFAULT '0',
  `checkinId` int(11) DEFAULT '-1',
  PRIMARY KEY (`roomlogId`),
  KEY `RoomLogIndex` (`roomId`),
  KEY `RoomLogIDIndex` (`roomlogId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roomlogs
-- ----------------------------
BEGIN;
INSERT INTO `roomlogs` VALUES ('FL001', 'test', '2018-08-13 18:23:27', '2018-08-13 18:23:27', 1, 1, 0, 1);
INSERT INTO `roomlogs` VALUES ('FL001', 'test', '2018-08-12 18:23:27', '2018-08-12 18:23:27', 2, 1, 0, 2);
INSERT INTO `roomlogs` VALUES ('FL001', 'test', '2018-08-11 18:23:27', '2018-08-11 18:23:27', 3, 1, 0, 3);
INSERT INTO `roomlogs` VALUES ('FL001', 'test', '2018-08-15 18:23:27', '1970-01-18 13:11:50', 4, 1, 0, -1);
INSERT INTO `roomlogs` VALUES ('FL001', 'test', '2018-08-15 12:23:27', '2018-08-15 13:23:27', 5, 0, 0, -1);
INSERT INTO `roomlogs` VALUES ('FL001', NULL, '1970-01-18 13:11:40', '1970-01-18 13:41:40', 6, 1, 0, 4);
INSERT INTO `roomlogs` VALUES ('FL001', 'test', '2017-06-20 18:23:27', NULL, 7, 1, 0, -1);
COMMIT;

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomname` varchar(50) DEFAULT NULL,
  `roomId` varchar(200) NOT NULL,
  `building` varchar(100) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `lng` double DEFAULT '-76.1373045',
  `threshold` int(11) DEFAULT '180',
  `code` varchar(10) DEFAULT 'NO',
  `lat` double DEFAULT '43.0391534',
  PRIMARY KEY (`id`,`roomId`),
  KEY `RoomIndex` (`roomId`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rooms
-- ----------------------------
BEGIN;
INSERT INTO `rooms` VALUES (1, 'Main Lounge', 'ML001', 'Booth Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (2, 'Main Lounge 2', 'ML002', 'Booth Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (3, 'Floor 1', 'FL001', 'Booth Hall', 1, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (4, 'Floor 2', 'FL002', 'Booth Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (5, 'Floor 3', 'FL003', 'Booth Hall', 1, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (6, 'Floor 4', 'FL004', 'Booth Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (7, 'Floor 5', 'FL005', 'Booth Hall', 1, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (8, 'Floor 6', 'FL006', 'Booth Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (9, 'Floor 7', 'FL007', 'Booth Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (10, 'Floor 8', 'FL008', 'Booth Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (15, '            ', 'HH205', 'SALTBox', 1, -76.133236, 180, 'NO', 43.038115);
INSERT INTO `rooms` VALUES (26, '105', 'BH105', 'Bowne Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (27, '108', 'BH108', 'Bowne Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (28, '100', 'CM100', 'Carnegie (Math)', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (29, '114', 'CM114', 'Carnegie (Math)', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (30, '200', 'CM200', 'Carnegie (Math)', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (31, '001', 'CH001', 'Crouse Hinds', 0, -76.136975, 180, 'NO', 43.040134);
INSERT INTO `rooms` VALUES (32, '017', 'CH017', 'Crouse Hinds', 0, -76.136975, 180, 'NO', 43.040134);
INSERT INTO `rooms` VALUES (33, '020', 'CH020', 'Crouse Hinds', 1, -76.136975, 180, 'YES', 43.040134);
INSERT INTO `rooms` VALUES (34, '101', 'CH101', 'Crouse Hinds', 0, -76.136975, 180, 'NO', 43.040134);
INSERT INTO `rooms` VALUES (35, '010', 'EH010', 'Egger Hall', 1, -76.1360063, 180, 'NO', 43.0380036);
INSERT INTO `rooms` VALUES (36, '111', 'EH111', 'Egger Hall', 0, -76.1360063, 180, 'NO', 43.0380036);
INSERT INTO `rooms` VALUES (37, '100', 'FC100', 'Falk College', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (38, '101', 'FC101', 'Falk College', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (39, '104', 'FC104', 'Falk College', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (40, '175', 'FC175', 'Falk College', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (41, '201', 'FC201', 'Falk College', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (42, '275', 'FC275', 'Falk College', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (43, '107', 'HL107', 'Hall of Languages', 0, -76.134519, 180, 'NO', 43.038451);
INSERT INTO `rooms` VALUES (44, '114', 'HL114', 'Hall of Languages', 0, -76.134519, 180, 'NO', 43.038451);
INSERT INTO `rooms` VALUES (45, '115', 'HL115', 'Hall of Languages', 0, -76.134519, 180, 'NO', 43.038451);
INSERT INTO `rooms` VALUES (46, '201', 'HL201', 'Hall of Languages', 0, -76.134519, 180, 'NO', 43.038451);
INSERT INTO `rooms` VALUES (47, '207', 'HL207', 'Hall of Languages', 0, -76.134519, 180, 'NO', 43.038451);
INSERT INTO `rooms` VALUES (48, '018', 'HH018', 'Hinds Hall', 1, -76.1335232, 180, 'NO', 43.0382095);
INSERT INTO `rooms` VALUES (49, '013', 'HH013', 'Hinds Hall', 1, -76.1335232, 180, 'NO', 43.0382095);
INSERT INTO `rooms` VALUES (50, '027', 'HH027', 'Hinds Hall', 1, -76.1335232, 180, 'NO', 43.0382095);
INSERT INTO `rooms` VALUES (51, '011', 'HH011', 'Hinds Hall', 1, -76.1335232, 180, 'NO', 43.0382095);
INSERT INTO `rooms` VALUES (52, '323A', 'HBC323', 'HBC', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (53, 'giff', 'HBCGIFF', 'HBC', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (54, 'kitt', 'HBCKITT', 'HBC', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (55, '105', 'HT105', 'Huntington Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (56, '001', 'LSB001', 'Life Science Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (57, '004', 'LSB004', 'Life Science Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (58, '011', 'LSB011', 'Life Science Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (59, '105', 'LSB105', 'Life Science Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (60, '200', 'LSB200', 'Life Science Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (61, '300', 'LSB300', 'Life Science Building', 1, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (62, '105', 'LH105', 'Link Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (63, '229', 'LYH229', 'Lyman Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (64, '202A', 'MSM202A', 'Marshall Square Mall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (65, '205B', 'MSM205B', 'Marshall Square Mall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (66, '205C', 'MSM205C', 'Marshall Square Mall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (67, '110', 'MH110', 'Maxwell Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (68, 'AUD', 'MHAUD', 'Maxwell Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (69, '101', 'NCC101', 'Newhouse Comm Center', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (70, '102', 'NCC102', 'Newhouse Comm Center', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (71, '469', 'NCC469', 'Newhouse Comm Center', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (72, '250', 'NCC250', 'Newhouse Comm Center', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (73, '141', 'NCC141', 'Newhouse Comm Center', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (74, '104N', 'PB104N', 'Physics Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (75, '121', 'SAB121', 'Shaffer Art Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (76, '205', 'SAB205', 'Shaffer Art Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (77, 'AUD', 'SABAUD', 'Shaffer Art Building', 0, -76.1373045, 180, 'YES', 43.0391534);
INSERT INTO `rooms` VALUES (78, '241', 'SH241', 'Sims Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (79, '331', 'SH331', 'Sims Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (80, '437', 'SH437', 'Sims Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (81, '101', 'SLH101', 'Slocum Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (82, '104', 'SLH104', 'Slocum Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (83, '227', 'SMH227', 'Smith Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (84, '330', 'SMH330', 'Smith Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (85, '337', 'SMH337', 'Smith Hall', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (86, '104', 'THB104', 'Tolley Humanities Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (87, '001', 'WSM001', 'Whitman', 1, -76.1339859, 180, 'NO', 43.0421635);
INSERT INTO `rooms` VALUES (88, '003', 'WSM003', 'Whitman', 0, -76.1339859, 180, 'NO', 43.0421635);
INSERT INTO `rooms` VALUES (89, '004', 'WSM004', 'Whitman', 0, -76.1339859, 180, 'NO', 43.0421635);
INSERT INTO `rooms` VALUES (90, '007', 'WSM007', 'Whitman', 0, -76.1339859, 180, 'NO', 43.0421635);
INSERT INTO `rooms` VALUES (91, '103', 'WSM103', 'Whitman', 0, -76.1339859, 180, 'NO', 43.0421635);
INSERT INTO `rooms` VALUES (92, '211', 'WB211', 'Women\'s Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (93, '303', 'WB303', 'Women\'s Building', 0, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (94, 'Room', 'GYM', 'GYM', 1, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (95, '1st Floor', 'BLFL001', 'Bird Library', 1, -76.1373045, 180, 'NO', 43.0391534);
INSERT INTO `rooms` VALUES (97, '205', 'HL205', 'Hall of Languages', 0, -76.134519, 180, 'YES', 43.038451);
INSERT INTO `rooms` VALUES (98, 'Grant Aud', 'WHAUD', 'White Hall', 1, -76.1374024, 180, 'YES', 43.0369066);
INSERT INTO `rooms` VALUES (99, '111', 'HH111', 'Hinds Hall', 1, -76.133236, 180, 'NO', 43.038115);
INSERT INTO `rooms` VALUES (100, 'Class Room', 'HYAUD', 'Heroy Auditorium', 1, -76.1360743, 180, 'NO', 43.0373165);
INSERT INTO `rooms` VALUES (101, '010', 'CH010', 'Crouse Hinds', 1, -76.136975, 180, 'NO', 43.040134);
INSERT INTO `rooms` VALUES (102, '021', 'HH021', 'Hinds Hall', 1, -76.1335232, 180, 'YES', 43.0382095);
INSERT INTO `rooms` VALUES (103, '117', 'HH117', 'Hinds Hall', 1, -76.1335232, 180, 'YES', 43.0382095);
INSERT INTO `rooms` VALUES (104, '250', 'NH3250', 'New House 3', 1, -76.135871, 180, 'NO', 43.039932);
COMMIT;

-- ----------------------------
-- Table structure for rosters
-- ----------------------------
DROP TABLE IF EXISTS `rosters`;
CREATE TABLE `rosters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(50) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `courseNumber` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rosters
-- ----------------------------
BEGIN;
INSERT INTO `rosters` VALUES (1, 'te', 'st', 'test', 'testnumber');
COMMIT;

-- ----------------------------
-- Table structure for semester
-- ----------------------------
DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester` (
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user_pic
-- ----------------------------
DROP TABLE IF EXISTS `user_pic`;
CREATE TABLE `user_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `netId` varchar(30) DEFAULT NULL,
  `picName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_pic
-- ----------------------------
BEGIN;
INSERT INTO `user_pic` VALUES (1, 'test', '27697f43-13d8-45a8-a223-d0fbbd8728fa.jpg');
COMMIT;

-- ----------------------------
-- Table structure for userdevice
-- ----------------------------
DROP TABLE IF EXISTS `userdevice`;
CREATE TABLE `userdevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `device` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for userevents
-- ----------------------------
DROP TABLE IF EXISTS `userevents`;
CREATE TABLE `userevents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `category` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `date` varchar(50) DEFAULT NULL,
  `timebegin` varchar(50) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `desc` text NOT NULL,
  `location` varchar(200) DEFAULT NULL,
  `eventId` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT 'userevent',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dateinlong` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for userpreferences
-- ----------------------------
DROP TABLE IF EXISTS `userpreferences`;
CREATE TABLE `userpreferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(50) DEFAULT NULL,
  `categories` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of userpreferences
-- ----------------------------
BEGIN;
INSERT INTO `userpreferences` VALUES (1, 'test', '[{\"category\":\"Academic\",\"isSelected\":true,\"selected\":true},{\"category\":\"Other\",\"isSelected\":true,\"selected\":true},{\"category\":\"Social\",\"isSelected\":true,\"selected\":true},{\"category\":\"Health\",\"isSelected\":true,\"selected\":true},{\"category\":\"Entertainment\",\"isSelected\":true,\"selected\":true},{\"category\":\"Work\",\"isSelected\":true,\"selected\":true}]');
COMMIT;

-- ----------------------------
-- Table structure for userprofile
-- ----------------------------
DROP TABLE IF EXISTS `userprofile`;
CREATE TABLE `userprofile` (
  `gender` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `summary` text,
  `description` text,
  `intentions` varchar(100) DEFAULT NULL,
  `netId` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(100) DEFAULT NULL,
  `major` varchar(50) DEFAULT NULL,
  `hometown` varchar(50) DEFAULT NULL,
  `anonymity` int(11) DEFAULT '0',
  `optout` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`netId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userprofile
-- ----------------------------
BEGIN;
INSERT INTO `userprofile` VALUES ('male', 20, 'I am summary', 'I am description', 'I am an intention', 'test', 1, 'test', 'Computer Science', 'syracuse', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `fullName` varchar(100) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `qrcode` varchar(100) DEFAULT NULL,
  `accessToken` text,
  `role` varchar(50) DEFAULT NULL,
  `netid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- View structure for nontestingusers
-- ----------------------------
DROP VIEW IF EXISTS `nontestingusers`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `nontestingusers` AS select `records`.`id` AS `id`,`records`.`event` AS `event`,`records`.`platform` AS `platform`,`records`.`device` AS `device`,`records`.`description` AS `description`,`records`.`data` AS `data`,`records`.`createTime` AS `createTime`,`records`.`createTimeOfSys` AS `createTimeOfSys`,`records`.`lat` AS `lat`,`records`.`lng` AS `lng` from `records` where `records`.`device` in (select `apikeys`.`device` from `apikeys` where (`apikeys`.`email` not in ('ysun102@syr.edu','cniu@syr.edu','zli154@syr.edu','abrigand@syr.edu','sralain@syr.edu','yhuang@syr.edu','xguo13@syr.edu','ezhuang@syr.edu','lhuang16@syr.edu','cwpreuss@syr.edu','skhawaja@syr.edu','yyao08@syr.edu','qwang11@syr.edu','jlbort@syr.edu','xzhang97@syr.edu','yhuan114@syr.edu','zwang151@syr.edu')));

-- ----------------------------
-- Procedure structure for addOrUpdateBeaconrecord
-- ----------------------------
DROP PROCEDURE IF EXISTS `addOrUpdateBeaconrecord`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `addOrUpdateBeaconrecord`(puuid varchar(100), pmajor varchar(50), pminor varchar(50), puser varchar(50), pdevice varchar(50), paccesstime bigint)
begin
	declare _row int;
	select count(*) into _row from beaconrecords where beaconrecords.device = pdevice and beaconrecords.major = pmajor
	and beaconrecords.minor = pminor and beaconrecords.uuid = puuid and beaconrecords.user = puser;
	if _row > 0 then
		update beaconrecords set beaconrecords.accesstime = paccesstime where beaconrecords.device = pdevice and beaconrecords.major = pmajor
		and beaconrecords.minor = pminor and beaconrecords.uuid = puuid and (paccesstime - accesstime)>1000*60*60*8;
	else 
		insert into beaconrecords (device,major,minor,uuid,user,accesstime) value(pdevice,pmajor,pminor,puuid,puser,paccesstime);
	end if;
	select ROW_COUNT() as result;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for addOrUpdatePhotorecord
-- ----------------------------
DROP PROCEDURE IF EXISTS `addOrUpdatePhotorecord`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `addOrUpdatePhotorecord`(puuid varchar(100), pmajor varchar(50), pminor varchar(50), puser varchar(50), pdevice varchar(50), paccesstime bigint)
begin
	declare _row int;
	select count(*) into _row from photorecords where photorecords.device = pdevice and photorecords.major = pmajor
	and photorecords.minor = pminor and photorecords.uuid = puuid and photorecords.user = puser;
	if _row > 0 then
		update photorecords set photorecords.accesstime = paccesstime where photorecords.device = pdevice and photorecords.major = pmajor
		and photorecords.minor = pminor and photorecords.uuid = puuid and (paccesstime - accesstime)>1000*60*60*8;
	else 
		insert into photorecords (device,major,minor,uuid,user,accesstime) value(pdevice,pmajor,pminor,puuid,puser,paccesstime);
	end if;
	select ROW_COUNT() as result;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for addOrUpdateUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `addOrUpdateUser`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `addOrUpdateUser`(pemail varchar(50), pfirstname varchar(50), plastname varchar(50), pgender varchar(10), pphone varchar(30), paddress varchar(100), ppicture varchar(200), paccesstoken text, prole varchar(10), pfullname varchar(100), pqrcode varchar(300), pnetid varchar(50))
begin
	DECLARE _flag int;
	DECLARE _row int;
	select count(*) into _flag from users where users.netid = pnetid;
	
	if _flag = 0 then
		delete from users where users.email = pemail;
		INSERT into users (users.accessToken,users.email,users.firstname,users.lastname,users.fullname,
		users.gender,users.phone,users.picture,users.qrcode,users.role,users.address,users.netid)
		values(paccesstoken,pemail,pfirstname,plastname,pfullname,pgender,pphone,ppicture,pqrcode,prole,paddress,pnetid);
		select  LAST_INSERT_ID() as result;
	else
		update users SET accesstoken=paccesstoken,firstname=pfirstname,lastname=plastname,users.email = pemail,
			fullname = pfullname,gender=pgender,phone=pphone,picture=ppicture,qrcode=pqrcode,role=prole,address=paddress 
			where users.netid = pnetid;
		if ROW_COUNT() > 0 then
			select users.id as result from users where users.email = pemail;
		ELSE
			select ROW_COUNT() as result;
		end if;
	end if;

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for addOrUpdateUserPic
-- ----------------------------
DROP PROCEDURE IF EXISTS `addOrUpdateUserPic`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `addOrUpdateUserPic`(pNetId varchar(30), pPicName varchar(100))
begin
	DECLARE _row int;
	select count(*) into _row from user_pic where user_pic.netId = pNetId;
	
	if _row = 0 then
		INSERT into user_pic (netId,picName)
		values(pNetId,pPicName);
	else
		update user_pic SET user_pic.picName = pPicName where user_pic.netId = pNetId;
	end if;
	select ROW_COUNT() as result;	
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for addOrUpdateUserPreferences
-- ----------------------------
DROP PROCEDURE IF EXISTS `addOrUpdateUserPreferences`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `addOrUpdateUserPreferences`(pemail varchar(50), pCate text)
begin
	DECLARE row_ int;
	select count(*) into row_ from userpreferences where userpreferences.userId = pemail;
	if row_ = 1 then
		update userpreferences set userpreferences.categories = pCate where userpreferences.userId = pemail;
	ELSE
		DELETE from userpreferences where userpreferences.userId = pemail;
		INSERT into userpreferences (userpreferences.userId,userpreferences.categories)value(pemail,pCate);
	end if;
		select id from userpreferences where userpreferences.userId = pemail;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_alias
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_alias`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_alias`(palias varchar(100), pLocationId int)
begin
	INSERT into alias(alias.alias,locationId)value(palias,pLocationId);
	SELECT LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_attendance
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_attendance`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_attendance`(pdevice varchar(50))
begin
	DECLARE row_ int;
	select count(*) into row_ from attendance where device = pdevice and TIMESTAMPDIFF(day,createTime,NOW())= 0; 
	if row_ < 2 then
		insert into attendance (device) value (pdevice);
	end if;
	select row_count() as result;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_beacon
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_beacon`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_beacon`(pbeaconName varchar(100), puuID varchar(100), pmajor varchar(50), pminor varchar(50),pbeaconNumber varchar(200))
begin
	declare _row int;
	select count(*) into _row from beaconv2 where beaconv2.beaconNumber = pbeaconNumber;
	if _row > 0 then
		select -1 as result;
	ELSE
		insert into beaconv2 (beaconv2.beaconName,beaconv2.UUID,beaconv2.major,beaconv2.minor,beaconv2.beaconNumber)value(pbeaconName,puuID,pmajor,pminor,pbeaconNumber);
		select LAST_INSERT_ID();
	end if;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_beacon_room
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_beacon_room`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_beacon_room`(proomId varchar(50), pbeaconNumber varchar(200))
begin
	declare _row int;
	delete from beaconroomrelations where beaconroomrelations.roomId = proomId;
	insert into beaconroomrelations (beaconroomrelations.beaconNumber,beaconroomrelations.roomId)value(pbeaconNumber,proomId);
	select LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_button
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_button`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_button`(IN pname varchar(200), IN pdesc text, IN pbid int)
begin
	declare row_ int;
	insert into buttons (buttons.name,buttons.description,buttons.button_id) values (pname,pdesc,pbid);
	SELECT ROW_COUNT() into row_;
	if row_ > 0 then
		SELECT LAST_INSERT_ID() as result;
	else
		select row_ as result;
	end if;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_call_back
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_call_back`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_call_back`(pContent text)
begin
	insert into callbacks (content) value (pContent);
	select LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_course
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_course`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_course`(pname varchar(50), pCourseID varchar(50), psection varchar(20), pstartDate timestamp, pendDate timestamp, pstartTime varchar(50), pendTime varchar(50), proomId varchar(50))
begin
	DECLARE _flag int;
	DECLARE _row int;
	select count(*) into _flag from courses where courses.courseID = pCourseID;
	
	if _flag = 0 then
		INSERT into courses (courses.courseName,courses.courseID,courses.sectionNumber,courses.startDate,courses.endDate,courses.startTime,courses.endTime,courses.roomId)
		values(pname,pCourseID,psection,pstartDate,pendDate,pstartTime,pendTime,proomId);
		select  LAST_INSERT_ID() as result;
	else
		update courses SET courses.courseName = pname,sectionNumber = psection,startDate=pstartDate,endDate=pendDate,startTime=pstartTime,endTime=pendTime,roomId=proomId where courses.courseID = pCourseID;
		if ROW_COUNT() > 0 then
			select courses.id as result from courses where courses.courseID = pCourseID;
		ELSE
			select ROW_COUNT() as result;
		end if;
	end if;


	
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_event
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_event`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_event`(pdate varchar(50), ptitle varchar(150), pdesc text, plocation varchar(200), ptimebegin varchar(50), ptimeend varchar(50), pcategory varchar(50), pphone varchar(50), pprice text, psponsor text, pcontact varchar(50), pemail varchar(50), plat double, plng double, peventId varchar(100), pdayinlong bigint, pcapacity varchar(50))
BEGIN
	declare _count int;
	select count(*) into _count from `events` where `events`.eventId = peventId;
	if _count > 0 THEN
		update `events` set `events`.date = pdate,`events`.title = ptitle,`events`.desc = pdesc, 
		`events`.location = plocation, `events`.timebegin = ptimebegin, `events`.timeend = ptimeend,
		`events`.category = pcategory, `events`.phone = pphone, `events`.price = pprice, `events`.sponsor = psponsor,
			`events`.contact = pcontact, `events`.email = pemail, `events`.lat = plat, `events`.lng = plng, `events`.eventId = peventId
		,`events`.dateinlong = pdayinlong,`events`.capacity = pcapacity
		where `events`.eventId = peventId;
		select `events`.id from `events` where `events`.eventId = peventId;
	else
		INSERT into `events` (`events`.date,title,`events`.desc,location,timebegin,timeend,category,
		phone,price,sponsor,contact,email,lat,lng,eventId,`events`.dateinlong,`events`.capacity)value(pdate,ptitle,pdesc,plocation,ptimebegin,
		ptimeend,pcategory,pphone,pprice,psponsor,pcontact,pemail,plat,plng,peventId,pdayinlong,pcapacity);
		SELECT LAST_INSERT_ID() as result;
	end if;

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_firebase_token
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_firebase_token`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_firebase_token`(ptoken text, pdevice varchar(100), pplatform varchar(20), pemail varchar(50), pnetid varchar(50))
begin
 
 DECLARE counter int;

 
 select count(*) into counter from firebasetokens where firebasetokens.device = pdevice;

 if counter > 0 then
	update firebasetokens set token = ptoken,platform = pplatform, email = pemail, netid = pnetid where device = pdevice;
 else
	INSERT into firebasetokens (firebasetokens.token,firebasetokens.device,firebasetokens.platform,email,netid)value(ptoken,pdevice,pplatform,pemail,pnetid);
 end if;
 
 select LAST_INSERT_ID();
	
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_key
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_key`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_key`(IN deviceid varchar(100), IN apiKey text, IN expire date, IN pplatform varchar(20), pemail varchar(50), pnetid varchar(50))
begin
		delete from apikeys where device = deviceid;
		insert into apikeys (device,apikey,expiretime,platform,email,netid)values(deviceid,apiKey,expire,pplatform,pemail,pnetid);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_leader_board_record
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_leader_board_record`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_leader_board_record`(proomId varchar(200), pcount int)
BEGIN
INSERT into leaderboard (roomId,count)value(proomId,pcount);
SELECT LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_location
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_location`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_location`(IN pStatus int,IN plocation varchar(200), IN plat double, IN plng double, IN plevel varchar(50), IN poffice varchar(50), IN pcategory varchar(50), IN palt double, IN pType varchar(10), palias varchar(100))
begin
	declare row_ int;
	declare result_ int;
	
	select count(*) into row_ from locations where locations.address = plocation;

	if row_ = 0 then

		insert into locations 
			(locations.address,locations.lat,locations.lng,locations.status,locations.roomNo,locations.floor,locations.category,
		locations.alt,locations.locationType,locations.alias)
			values
			(plocation,plat,plng,pStatus,poffice,plevel,pcategory,palt,pType,palias);
		SELECT LAST_INSERT_ID() as result;
	ELSE
		
		SELECT locations.id from locations where  locations.address = plocation;
	end if;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_log
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_log`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_log`(IN puid int, IN pbutton int, IN pparam text, IN purl text, IN pdate varchar(200), IN plat double, IN plng double, IN pdevice varchar(200))
begin
	declare row_ int;
		insert into logs (logs.user_id,logs.button_id,logs.parameters,logs.url,logs._date,logs.lat,logs.lng,logs.device_id) 
			values (puid,pbutton,pparam,purl,pdate,plat,plng,pdevice);
	SELECT ROW_COUNT() into row_;
	if row_ > 0 then
		SELECT LAST_INSERT_ID() as result;
	else
		select row_ as result;
	end if;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_new_message
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_new_message`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_new_message`(psender varchar(50), preceiver varchar(50), pcontent text, pemail varchar(10))
begin

	INSERT into messages (messages.sender,messages.receiver,messages.content,messages.email)value(psender,preceiver,pcontent,pemail);
	select LAST_INSERT_ID();

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_or_update_user_profile
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_or_update_user_profile`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_or_update_user_profile`(pnetid varchar(50), pgender varchar(50), page int, psummary text, pintentions varchar(100), pdesc text, pnick varchar(100), pmajor varchar(50), phome varchar(50), panon int, poptout int)
begin
	DECLARE _flag int;
	DECLARE _row int;
	select count(*) into _flag from userprofile where userprofile.netId = pnetid;
	
	if _flag = 0 then
		INSERT into userprofile
		(userprofile.netId,userprofile.gender,userprofile.age,userprofile.summary,userprofile.intentions,userprofile.description,userprofile.nickname,userprofile.major,userprofile.hometown,userprofile.anonymity,userprofile.optout)
		values(pnetid,pgender,page,psummary,pintentions,pdesc,pnick,pmajor,phome,panon,poptout);
		select  LAST_INSERT_ID() as result;
	else
		update userprofile SET userprofile.gender =pgender,userprofile.age = page,userprofile.summary = psummary,userprofile.intentions = pintentions,userprofile.description = pdesc,userprofile.nickname = pnick,userprofile.major = pmajor,userprofile.hometown = phome,userprofile.anonymity=panon,userprofile.optout = poptout
			where userprofile.netid = pnetid;
		select userprofile.id from userprofile where userprofile.netid = pnetid;
	end if;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_photo
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_photo`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_photo`(pLat double, pLng double, pSuf varchar(100), pName varchar(200), pLocation varchar(255), pLocationId int, palt double)
begin
	insert into photos (photos.name,photos.lat,photos.lng,photos.suffix,photos.location,photos.locationId,photos.alt)values(pName,pLat,pLng,pSuf,pLocation,pLocationId,palt);
	select LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_photov2
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_photov2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_photov2`(pphotoname varchar(200), plat double, plng double, pfeeling text, pplace varchar(300), pdesc text, pnetid varchar(50))
begin
	insert into photosv2 (photosv2.photoname,photosv2.lat,photosv2.lng,photosv2.feeling,photosv2.place,photosv2.description,netid)values(pphotoname,pLat,pLng,pfeeling,pplace,pdesc,pnetid);
	select LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_records
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_records`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_records`(pEvent varchar(100), pDescription text, pCreateTime datetime, pPlatform varchar(50), pDevice varchar(100), pData text, pLat double, pLng double)
begin
	INSERT into records 
			(records.event,records.description,records.createTime,records.platform,records.device,records.data,records.lat,records.lng)
		value
			(pEvent,pDescription,pCreateTime,pPlatform,pDevice,pData,pLat,pLng);
	
	select LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_to_calendar
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_to_calendar`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_to_calendar`(pType varchar(20), pnetid varchar(50), pTag varchar(100))
begin
	declare count_ int;
	select count(*) into count_ from calendar where calendar.tag = pTag and calendar.netid = pnetid;
	if count_ = 0 then
		INSERT into calendar (calendar.type,calendar.tag,calendar.netid)value(pType,pTag,pnetid);
	end if;
	select e.* from `events` e,calendar c where e.eventId = c.tag and c.netid = pnetid and c.type = 'event';
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_to_roster
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_to_roster`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_to_roster`(plast varchar(20), pfirst varchar(20), puser varchar(50), pcourse varchar(50))
begin
	declare _row int;
	select count(*) into _row from rosters where rosters.userName = puser and rosters.courseNumber = pcourse;
	if _row > 0 then
		select -10 as result;
	ELSE
		insert into rosters (rosters.lastName,rosters.firstName,rosters.userName,rosters.courseNumber)value(plast,pfirst,puser,pcourse);
		select LAST_INSERT_ID();
	end if;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_unlocate_address
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_unlocate_address`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_unlocate_address`(IN plocation varchar(200))
begin
	declare row_ int;
	select count(*) into row_ from locations where locations.address = plocation;
	if row_ = 0 then
		insert into locations (locations.address) values (plocation);
		SELECT ROW_COUNT() into row_;
		if row_ > 0 then
			SELECT LAST_INSERT_ID() as result;
		end if;
		
	else
		set row_ = 0;
	end if;
	select row_ as result;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for add_user_event
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_user_event`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `add_user_event`(IN `ptitle` varchar(150), IN `pcategory` varchar(50), IN `pphone` varchar(50), IN `pemail` varchar(50), IN `ptimebegin` varchar(50), IN `plat` double, IN `plng` double, IN `pdesc` text, IN `plocation` varchar(200), IN `peventId` varchar(100), IN `pdate` varchar(50), IN `puseremail` varchar(50), IN `pdateinlong` bigint, IN `ptimeend` varchar(50), IN `pType` varchar(20), IN `pCapacity` varchar(50))
begin
	INSERT INTO events (title,category,phone,email,events.date,timebegin,lat,lng,events.desc,location,eventId,events.netid,events.dateinlong,events.timeend,events.type,events.capacity)
			value
				(ptitle,pcategory,pphone,pemail,pdate,ptimebegin,plat,plng,pdesc,plocation,peventId,puseremail,pdateinlong,ptimeend,pType,pCapacity);
	select LAST_INSERT_ID() as result;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for all_results
-- ----------------------------
DROP PROCEDURE IF EXISTS `all_results`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `all_results`()
begin
DROP TABLE IF EXISTS `all_results_mf`;
CREATE TABLE all_results_mf

	select Q1.*,Q2.firstTimeDetected,Q2.countOfSendEmail,Q2.receiverIsStranger,Q2.openAppTimes
	#,Q2.CheckEventDetailsFromMap,Q2.CheckEventDetailsFromList,Q2.TapOtherUserProfile
	from (select data_analysis_result_v3.netid,data_analysis_result_v3.courseNumber,data_analysis_result_v3.platform,
	sum(data_analysis_result_v3.CountOfFirstCancelLocation) as CountOfFirstCancelLocation,
	sum(data_analysis_result_v3.CountOfLaterCancelLocation) as CountOfLaterCancelLocation,
	sum(data_analysis_result_v3.CountOfBluetoothOff) as CountOfBluetoothOff,
	sum(data_analysis_result_v3.CountOfBroadcastOFF) as CountOfBroadcastOFF,
	sum(data_analysis_result_v3.CountOfTabSocialTabButton) as CountOfTabSocialTabButton,
	sum(data_analysis_result_v3.CountOfTabSocialTabButtonInClass) as CountOfTabSocialTabButtonInClass,
	sum(data_analysis_result_v3.CountOfTabSocialTabButtonOutClass) as CountOfTabSocialTabButtonOutClass,
	sum(data_analysis_result_v3.CountOfNowButtonPressed) as CountOfNowButtonPressed,
	sum(data_analysis_result_v3.CountOfNowButtonPressedInClass) as CountOfNowButtonPressedInClass,
	sum(data_analysis_result_v3.CountOfNowButtonPressedOutClass) as CountOfNowButtonPressedOutClass,
	sum(data_analysis_result_v3.CountOfPastButtonPressed) as CountOfPastButtonPressed,
	sum(data_analysis_result_v3.CountOfPastButtonPressedInClass) as CountOfPastButtonPressedInClass,
	sum(data_analysis_result_v3.CountOfPastButtonPressedOutClass) as CountOfPastButtonPressedOutClass,
	sum(data_analysis_result_v3.TapOtherUserProfile) as TapOtherUserProfile,
	sum(data_analysis_result_v3.CountOfTapEventListTabButton) as CountOfTapEventListTabButton,
	sum(data_analysis_result_v3.CheckEventDetailsFromMap) as CheckEventDetailsFromMap,
	sum(data_analysis_result_v3.CheckEventDetailsFromList) as CheckEventDetailsFromList,
	sum(data_analysis_result_v3.countOfDetectedByOtherBeacons) as countOfDetectedByOtherBeacons,
	data_analysis_result_v3.autoCheckIn,
	data_analysis_result_v3.manuallyCheckIn,
	min(data_analysis_result_v3.firstlyInstalled) as firstlyInstalled,
	sum(data_analysis_result_v3.CountOfAppInitiatedByNotification) as CountOfAppInitiatedByNotification
	from data_analysis_result_v3 group by data_analysis_result_v3.netid) Q1 , 
	

	(select data_analysis_result2_v3.netid,data_analysis_result2_v3.courseNumber,
	data_analysis_result2_v3.firstTimeDetected as firstTimeDetected,
	data_analysis_result2_v3.countOfSendEmail,
	data_analysis_result2_v3.receiverIsStranger,
	sum(data_analysis_result2_v3.openAppTimes) as openAppTimes
	#sum(data_analysis_result2_v3.) as CheckEventDetailsFromMap,
	#sum(data_analysis_result2_v3.CheckEventDetailsFromList) as CheckEventDetailsFromList,
	#sum(data_analysis_result2_v3.TapOtherUserProfile) as TapOtherUserProfile

	from data_analysis_result2_v3 group by data_analysis_result2_v3.netid) Q2 where Q1.netid = Q2.netid and Q1.courseNumber = Q2.courseNumber;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for approve_deny_event
-- ----------------------------
DROP PROCEDURE IF EXISTS `approve_deny_event`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `approve_deny_event`(pevent int, pname varchar(100), pcomment text, pdeci varchar(20))
begin

insert into approvedenyevent (approvedenyevent.eventId,approvedenyevent.`name`,approvedenyevent.`comment`,approvedenyevent.decision) value (pevent,pname,pcomment,pdeci);
select LAST_INSERT_ID();

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for beacons_detected_between_sep_and_dec
-- ----------------------------
DROP PROCEDURE IF EXISTS `beacons_detected_between_sep_and_dec`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `beacons_detected_between_sep_and_dec`()
begin
select beaconroomrelations.roomId,beaconv2.*from beaconroomrelations,beaconv2 , (select * from roomlogs where (MONTH(roomlogs.enterTime) > 8 and MONTH(roomlogs.enterTime) <12)  
							or ( MONTH(roomlogs.exitTime) > 8 and MONTH(roomlogs.exitTime) < 12) group by roomId) as result1
where result1.roomId = beaconroomrelations.roomId and beaconroomrelations.beaconNumber = beaconv2.beaconNumber
group by beaconv2.beaconNumber;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for block_user
-- ----------------------------
DROP PROCEDURE IF EXISTS `block_user`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `block_user`(pnetId varchar(50), pCreateBy varchar(50))
begin
	INSERT into blacklist(blacklist.netId,blacklist.createdBy)value(pnetId,pCreateBy);
	select LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for change_event_status
-- ----------------------------
DROP PROCEDURE IF EXISTS `change_event_status`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `change_event_status`(psta int, peventId int)
begin
	update events set events.`status` = psta where events.id = peventId;
	select ROW_COUNT();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for change_opt
-- ----------------------------
DROP PROCEDURE IF EXISTS `change_opt`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `change_opt`(pnetId varchar(50), pvalue int)
begin

update userprofile set userprofile.optout = pvalue where userprofile.netId = pnetId;
select ROW_COUNT();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_anonymity
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_anonymity`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_anonymity`(pnetId varchar(50))
begin

	select userprofile.anonymity from userprofile where userprofile.netId = pnetId;

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_attendance
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_attendance`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_attendance`(pdevice varchar(50))
begin
	select count(*) from  attendance where device = pdevice and TIMESTAMPDIFF(hour,createTime,NOW()) < 24 and TIMESTAMPDIFF(hour,createTime,NOW()) > 0;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_attendance_for_M003
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_attendance_for_M003`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_attendance_for_M003`(pbegin datetime, pend datetime, pcn varchar(20))
begin

	declare proomId varchar(50);
	
	select courseV2.roomId into proomId from courseV2 where courseV2.courseNumber = pcn limit 0,1;

	select result2.*,DATE_FORMAT(result2.time,'%k:%i') as enterTime,roomcheckin.code as codes from(
	select t_roster.*,result.enterTime as time,result.checkinId from (select * from rosters where rosters.courseNumber = pcn) t_roster  left join 
	(select * from roomlogs where DATEDIFF(roomlogs.enterTime,pbegin) = 0 and roomlogs.roomId = proomId
	
		and
		(
					DATE_ADD(roomlogs.enterTime, INTERVAL 11 MINUTE) > pbegin
					or 
					(roomlogs.enterTime > pbegin and roomlogs.enterTime < pend) 
					or (roomlogs.exitTime > pbegin and roomlogs.exitTime<pend)
					or (roomlogs.enterTime < pbegin and roomlogs.exitTime > pend)  
					or (roomlogs.enterTime < pbegin and roomlogs.exitTime is null and now() > pbegin)
		) group by roomlogs.netId
	) result on  t_roster.userName = result.netId order by t_roster.lastName) result2 left join roomcheckin on result2.checkinId = roomcheckin.id;
		
				
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_attendance_test
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_attendance_test`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_attendance_test`(pbegin datetime, pend datetime, pcn varchar(20))
begin

	declare proomId varchar(50);
	
	select courseV2.roomId into proomId from courseV2 where courseV2.courseNumber = pcn limit 0,1;

	select result2.*,DATE_FORMAT(result2.time,'%k:%i') as enterTime,roomcheckin.code as codes from(
	select t_roster.*,result.enterTime as time,result.checkinId from (select * from rosters where rosters.courseNumber = pcn) t_roster  left join 
	(select * from roomlogs where DATEDIFF(roomlogs.enterTime,pbegin) = 0 and roomlogs.roomId = proomId
	
		and
		(
					(roomlogs.enterTime > pbegin and roomlogs.enterTime < pend) 
					or (roomlogs.exitTime > pbegin and roomlogs.exitTime<pend)
					or (roomlogs.enterTime < pbegin and roomlogs.exitTime > pend)  
					or (roomlogs.enterTime < pbegin and roomlogs.exitTime is null and now() > pbegin)
		) group by roomlogs.netId
	) result on  t_roster.userName = result.netId order by t_roster.lastName) result2 left join roomcheckin on result2.checkinId = roomcheckin.id;
		
				
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_attendance_V2
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_attendance_V2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_attendance_V2`(pbegin datetime, pend datetime, pcn varchar(20))
begin

	
	select *,
	(select DATE_FORMAT(roomlogs.enterTime,'%k:%i') from roomlogs where ((roomlogs.enterTime > pbegin and roomlogs.enterTime < pend) or (roomlogs.exitTime > pbegin and roomlogs.exitTime<pend)  or (roomlogs.enterTime < pbegin and roomlogs.exitTime > pend)  or (roomlogs.enterTime < pbegin and roomlogs.exitTime is null and now() > pbegin) )  and roomlogs.roomId = roster_course.roomId and roomlogs.netId = roster_course.userName order by roomlogs.roomlogId limit 0,1) as enterTime 
	,(select roomlogs.checkinId from roomlogs where ((roomlogs.enterTime > pbegin and roomlogs.enterTime < pend) or (roomlogs.exitTime > pbegin and roomlogs.exitTime<pend)  or (roomlogs.enterTime < pbegin and roomlogs.exitTime > pend) or (roomlogs.enterTime < pbegin and roomlogs.exitTime is null and now() > pbegin))  and roomlogs.roomId = roster_course.roomId and roomlogs.netId = roster_course.userName AND roomlogs.checkinId > 0 order by roomlogs.roomlogId limit 0,1) as checkinId,
		(select roomcheckin.`code` from roomlogs,roomcheckin where roomlogs.checkinId = roomcheckin.id and  ((roomlogs.enterTime > pbegin and roomlogs.enterTime < pend) or (roomlogs.exitTime > pbegin and roomlogs.exitTime<pend)  or (roomlogs.enterTime < pbegin and roomlogs.exitTime > pend) or (roomlogs.enterTime < pbegin and roomlogs.exitTime is null and now() > pbegin))  and roomlogs.roomId = roster_course.roomId and roomlogs.netId = roster_course.userName AND roomlogs.checkinId > 0 order by roomlogs.roomlogId limit 0,1) as codes 
	from (select rosters.*,coursev2.roomId from rosters,coursev2 where rosters.courseNumber = pcn and rosters.courseNumber = coursev2.courseNumber)roster_course;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_blacklist
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_blacklist`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_blacklist`(senderId varchar(50), receiverId varchar(50))
begin

	select count(*) from blacklist where blacklist.netId = senderId and blacklist.createdBy = receiverId;

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_event_app_den
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_event_app_den`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_event_app_den`(peventid int)
begin
	select approvedenyevent.`comment`,approvedenyevent.`name`,approvedenyevent.creatTime,approvedenyevent.decision from approvedenyevent where approvedenyevent.eventId = peventid order by id desc limit 0,1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_if_exist_room
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_if_exist_room`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_if_exist_room`(proomId varchar(50))
BEGIN
	select count(*) from rooms where roomId = proomId;
END;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_message_timeline
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_message_timeline`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_message_timeline`(pnetId varchar(50))
begin
	select count(*) from messages where TIMESTAMPDIFF(MINUTE,messages.createTime,NOW()) < 5 and messages.sender = pnetId;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for check_room_code
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_room_code`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `check_room_code`(ptime timestamp, pcourse varchar(50))
begin
	select checkincodes.`code` from checkincodes where DATEDIFF(ptime,checkincodes.time) = 0 and checkincodes.courseId = pcourse limit 0,1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for clear_leader_board
-- ----------------------------
DROP PROCEDURE IF EXISTS `clear_leader_board`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `clear_leader_board`()
begin
update leaderboard set leaderboard.`status` = 1;
SELECT ROW_COUNT();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for count_for_leader_board
-- ----------------------------
DROP PROCEDURE IF EXISTS `count_for_leader_board`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `count_for_leader_board`(proomId varchar(200), pint int)
BEGIN

	select count(distinct netId) from roomlogs where roomlogs.roomId = proomId and DATEDIFF(NOW(),roomlogs.enterTime) = pint and roomlogs.hidden!=1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for create_new_room_code
-- ----------------------------
DROP PROCEDURE IF EXISTS `create_new_room_code`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `create_new_room_code`(pcode int, ptime timestamp, pcourse varchar(50))
BEGIN
	INSERT into checkincodes (checkincodes.`code`,checkincodes.time,checkincodes.courseId)value(pcode,ptime,pcourse);
	select LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for delete_beacon_record
-- ----------------------------
DROP PROCEDURE IF EXISTS `delete_beacon_record`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `delete_beacon_record`(pemail varchar(50))
begin
delete from beaconrecords where beaconrecords.`user` = pemail;
select ROW_COUNT() as result;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for delete_button_by_id
-- ----------------------------
DROP PROCEDURE IF EXISTS `delete_button_by_id`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `delete_button_by_id`(IN pid int)
begin
	declare row_ int;
	delete from buttons where id = pid;
	SELECT ROW_COUNT() into row_;
	select row_ as result;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for delete_button_by_name
-- ----------------------------
DROP PROCEDURE IF EXISTS `delete_button_by_name`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `delete_button_by_name`(IN pname varchar(200))
begin
	declare row_ int;
	delete from buttons where buttons.name = pname;
	SELECT ROW_COUNT() into row_;
	select row_ as result;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for delete_stu_from_roster
-- ----------------------------
DROP PROCEDURE IF EXISTS `delete_stu_from_roster`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `delete_stu_from_roster`(pnetId varchar(50), pcourse varchar(50))
BEGIN
	delete from rosters where rosters.userName = pnetId and rosters.courseNumber = pcourse;
	select ROW_COUNT();
END;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for enter_room
-- ----------------------------
DROP PROCEDURE IF EXISTS `enter_room`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `enter_room`(IN `pRoomId` varchar(200), IN `pnetid` varchar(50), IN `ptime` datetime, IN `phidden` int)
begin
      update roomlogs set roomlogs.exitTime = ptime,roomlogs.`status` = 1  where roomlogs.netId = pnetid and roomlogs.`status` = 0 and
      DATEDIFF(ptime,roomlogs.enterTime) = 0;
      INSERT into roomlogs (roomlogs.roomId,roomlogs.netId,roomlogs.enterTime,roomlogs.hidden)value(pRoomId,pnetid,ptime,phidden);
      select * from roomlogs where roomlogs.roomlogId = LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for enter_room_v2
-- ----------------------------
DROP PROCEDURE IF EXISTS `enter_room_v2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `enter_room_v2`(IN pRoomId varchar(200), IN pnetid varchar(50), IN ptime datetime, IN phidden int, pcheckid int, pexit datetime)
begin
      update roomlogs set roomlogs.exitTime = ptime,roomlogs.`status` = 1  where roomlogs.netId = pnetid and roomlogs.`status` = 0 and
      DATEDIFF(ptime,roomlogs.enterTime) = 0;
      INSERT into roomlogs (roomlogs.roomId,roomlogs.netId,roomlogs.enterTime,roomlogs.exitTime,roomlogs.hidden,roomlogs.checkinId)value(pRoomId,pnetid,ptime,pexit,phidden,pcheckid);
      select * from roomlogs where roomlogs.roomlogId = LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for event_reminders
-- ----------------------------
DROP PROCEDURE IF EXISTS `event_reminders`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `event_reminders`()
begin
	select calendar.netid,`events`.*,calendar.id as calendarid ,apikeys.apikey
			from 
					calendar,apikeys,`events`
			where 
					apikeys.netid = calendar.netid 
				and 
					`events`.eventId = calendar.tag 
				and 
					TIMESTAMPDIFF(HOUR,NOW(),CONCAT(`events`.date," ",`events`.timebegin)) < 3  
			and
					TIMESTAMPDIFF(HOUR,NOW(),CONCAT(`events`.date," ",`events`.timebegin)) > 0
				;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for exit_room
-- ----------------------------
DROP PROCEDURE IF EXISTS `exit_room`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `exit_room`(ptime datetime, pid int)
begin
	update roomlogs set roomlogs.exitTime = ptime,roomlogs.`status` = 1 where roomlogs.roomlogId = pid;
	select * from roomlogs where roomlogs.roomlogId = pid;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for find_beacon_in_area
-- ----------------------------
DROP PROCEDURE IF EXISTS `find_beacon_in_area`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `find_beacon_in_area`(plat double, plng double)
begin
	select beaconv2.major,beaconv2.minor,beaconv2.UUID from beaconv2 where beaconv2.areaId in (select areas.areaId from areas where plat < areas.lattop and plat > areas.latbottom and plng > areas.longleft and plng < areas.longright);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for find_location_by_name
-- ----------------------------
DROP PROCEDURE IF EXISTS `find_location_by_name`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `find_location_by_name`(IN plocation varchar(200))
begin
	select plocation as name,lat,lng from locations where 
	locations.address = plocation 
	or 
	locations.alias = plocation
	limit 0,1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getAddressByName
-- ----------------------------
DROP PROCEDURE IF EXISTS `getAddressByName`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getAddressByName`(plocation varchar(200))
begin
	select * from locations where locations.address = plocation or locations.alias = plocation limit 0,1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getAlbum
-- ----------------------------
DROP PROCEDURE IF EXISTS `getAlbum`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getAlbum`(IN startId int)
begin
	if startId >0 then
	select locations.*,photos.`name` as cover from locations,photos 
	where 		
		locations.id = photos.locationId and locations.id < startId
		GROUP BY locations.id desc limit 0,15;
	ELSE
		select locations.*,photos.`name` as cover from locations,photos 
	where 		
		locations.id = photos.locationId
		GROUP BY locations.id desc limit 0,15;
	end if;

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getKeys
-- ----------------------------
DROP PROCEDURE IF EXISTS `getKeys`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getKeys`()
begin
	select * from apikeys;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getUserPreference
-- ----------------------------
DROP PROCEDURE IF EXISTS `getUserPreference`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getUserPreference`(pemail varchar(50))
BEGIN
	select * from userpreferences where userpreferences.userId = pemail;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_all_buttons
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_all_buttons`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_all_buttons`()
begin
	select * from buttons;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_all_netids
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_all_netids`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_all_netids`()
begin
	select netid from apikeys group by netid;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_apikeys_by_netid
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_apikeys_by_netid`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_apikeys_by_netid`(IN `pnetid` varchar(50))
begin
	select * from apikeys where apikeys.netid = pnetid order by expiretime desc;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_beacon_location
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_beacon_location`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_beacon_location`(puuid varchar(100), pmajor varchar(50), pminor varchar(50))
begin
	select lat,lng,beacon.location as name from beacon where beacon.uuid = puuid and beacon.major = pmajor and beacon.minor = pminor;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_calendar_events
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_calendar_events`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_calendar_events`(pnetid varchar(50))
begin
	select * from `events` e,calendar c where e.eventId = c.tag and c.netid = pnetid;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_check_in_code
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_check_in_code`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_check_in_code`(proom varchar(50), ptime timestamp)
begin
select checkincodes.`code` from checkincodes,rooms,coursev2 where
 checkincodes.courseId = coursev2.courseNumber and coursev2.roomId = rooms.roomId and rooms.roomId = proom  and DATEDIFF(checkincodes.time,ptime) = 0 limit 0,1; 
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_count_of_leader_board
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_count_of_leader_board`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_count_of_leader_board`(proomId varchar(200))
begin
	select count from leaderboard where status = 0 and roomId = proomId limit 0,1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_coursev2
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_coursev2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_coursev2`(pcn varchar(20))
begin
	select * from coursev2 where coursev2.courseNumber = pcn;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_courseV2_by_roomId
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_courseV2_by_roomId`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_courseV2_by_roomId`(proom varchar(50))
begin
	select * from coursev2 where coursev2.roomId = proom limit 0,1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_course_info
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_course_info`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_course_info`(pnum varchar(100))
begin
	select * from courseV2 where courseNumber = pnum;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_course_schedule
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_course_schedule`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_course_schedule`(pnetId varchar(50))
begin
select coursev2.instructorEmail,coursev2.courseNumber,rooms.lat,rooms.lng,rooms.roomname,rooms.building,coursev2.instructor,coursev2.startTime,coursev2.endTime,coursev2.days,beaconv2.UUID,beaconv2.major,beaconv2.minor,rooms.roomId,coursev2.title,coursev2.abbr from 

rosters,coursev2,beaconroomrelations,beaconv2,rooms
 where 

coursev2.active = 'YES' 

AND
	rosters.courseNumber =coursev2.courseNumber

and
	beaconroomrelations.roomId = coursev2.roomId 

and
	rooms.roomId = coursev2.roomId

AND
	beaconroomrelations.beaconNumber = beaconv2.beaconNumber
	
AND  
	userName = pnetId;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_course_schedule_track
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_course_schedule_track`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_course_schedule_track`(pnetId varchar(50))
begin
	select * from courseScheduleTrack where courseScheduleTrack.netId = pnetId;

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_events_for_beacon
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_events_for_beacon`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_events_for_beacon`(plat double, plng double)
begin
	select *  
		from events where (2 * 6378.137*ASIN(SQRT(POW(SIN(PI()*(plat-events.lat)/360),2)+COS(PI()*plng/180)* 
											COS(events.lat * PI()/180)*POW(SIN(PI()*(plng-events.lng)/360),2))))  < 0.15
		and events.dateinlong > (UNIX_TIMESTAMP(NOW())*1000 - 1000*60*60*24);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_events_for_emotionmap
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_events_for_emotionmap`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_events_for_emotionmap`(pdate bigint)
begin
	select * from `events` where pdate <=`events`.dateinlong and `events`.status = 1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_event_by_id
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_event_by_id`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_event_by_id`(peventId int)
begin
	select * from events where events.id = peventId;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_event_for_event_notification_type_one
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_event_for_event_notification_type_one`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_event_for_event_notification_type_one`()
begin 
		select TIMESTAMPDIFF(hour,events.date,NOW()),events.* from events where TIMESTAMPDIFF(hour,events.date,NOW()) < 0 order by date ;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_most_recent_event
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_most_recent_event`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_most_recent_event`()
    DETERMINISTIC
begin
	select events.* from events where TIMESTAMPDIFF(day,events.date,NOW())= 0 
	and events.id not in (select event_pushed_marks.eventid from event_pushed_marks)
	order by date,timebegin limit 0,1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_most_recent_event_at_student_center
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_most_recent_event_at_student_center`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_most_recent_event_at_student_center`()
begin
select * from
(select (2 * 6378.137*ASIN(SQRT(POW(SIN(PI()*(43.040141 - lat)/360),2)+COS(PI()*-76.133742/180)* 

											COS(lat * PI()/180)*POW(SIN(PI()*(-76.133742-lng)/360),2))))  as distance,events.* from events) as pre
 where pre.distance <0.1 and TIMESTAMPDIFF(minute,pre.date,NOW()) < 24 and TIMESTAMPDIFF(hour,pre.date,NOW()) > 0 limit 0,1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_past_users_in_room
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_past_users_in_room`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_past_users_in_room`(IN `proom` varchar(200), IN `pday` int)
begin
select userprofile.netId,userprofile.nickname,
(select count(DISTINCT date(rr.enterTime)) from (select * from roomlogs rl1 where rl1.hidden!=1 and DATEDIFF(NOW(),
rl1.enterTime)< pday and rl1.roomId = proom) rr where rr.netId = roomlogs.netId) as num from roomlogs,userprofile where DATEDIFF(NOW(),roomlogs.enterTime)< 8 
and roomlogs.roomId = proom and userprofile.netId = roomlogs.netId group by netId order by num desc;

#select userprofile.netId,userprofile.nickname,
#(select count(DISTINCT date(rr.enterTime)) from (select * from roomlogs rl1 where rl1.hidden!=1 and DATEDIFF(NOW(),
#rl1.enterTime)< pday and rl1.roomId = proom) rr where rr.netId = roomlogs.netId) as num from roomlogs,userprofile 
#where roomlogs.roomId = 'flag' and DATEDIFF(NOW(),roomlogs.enterTime)< 8 
#and roomlogs.roomId = proom and userprofile.netId = roomlogs.netId group by netId order by num desc;


end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_photos
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_photos`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_photos`(pId int)
begin


	if pId > 0 THEN
		select * from photos where photos.id < pId order by photos.id desc LIMIT 0,30;
	ELSE

		select * from photos order by photos.id desc LIMIT 0,30;
	end if;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_photos_by_location
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_photos_by_location`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_photos_by_location`(pLocation int)
begin
	select * from photos where photos.locationId = pLocation;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_photo_by_place
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_photo_by_place`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_photo_by_place`(pplace varchar(300))
begin
	select * from photosv2 where place = pplace order by id desc;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_photo_covers
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_photo_covers`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_photo_covers`()
BEGIN
	select * from(SELECT * from photosv2 order by id desc) as tempTable GROUP BY tempTable.place order by id desc;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_picName
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_picName`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_picName`(pNetId varchar(30))
BEGIN
	SELECT user_pic.picName FROM user_pic where user_pic.netId = pNetId;
END;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_roomid
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_roomid`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_roomid`(pnum varchar(200))
begin
	select * from beaconRoomRelations where beaconRoomRelations.beaconNumber = pnum;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_rooms_needs_code
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_rooms_needs_code`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_rooms_needs_code`()
begin
	select * from rooms where rooms.`code` = 'YES';
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_room_details
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_room_details`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_room_details`(proom varchar(50))
BEGIN
	select * from rooms where rooms.roomId = proom;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_room_info
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_room_info`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_room_info`(proom varchar(200))
begin
	select roomlogs.roomId,roomlogs.enterTime,roomlogs.roomlogId,userprofile.* from roomlogs,userprofile where DATEDIFF(NOW(),roomlogs.enterTime) = 0 AND  roomlogs.`status` = 0 and roomlogs.roomId = proom and userprofile.netId = roomlogs.netId  and roomlogs.hidden!=1;


end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_room_infoV2
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_room_infoV2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_room_infoV2`(proom varchar(200))
begin
	select roomlogs.roomId,roomlogs.enterTime,roomlogs.roomlogId,userprofile.netId,userprofile.intentions,userprofile.nickname from roomlogs,userprofile,rooms rs where DATEDIFF(NOW(),roomlogs.enterTime) = 0 AND  roomlogs.`status` = 0 and TIMESTAMPDIFF(MINUTE,roomlogs.enterTime,NOW())< rs.threshold 
 and roomlogs.roomId =  proom and roomlogs.roomId = rs.roomId and userprofile.netId = roomlogs.netId and roomlogs.hidden!=1 ;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_room_with_photos
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_room_with_photos`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_room_with_photos`(proom varchar(200))
begin
	select roomlogs.roomId,roomlogs.enterTime,roomlogs.roomlogId,userprofile.*,user_pic.picName from roomlogs,userprofile,user_pic where DATEDIFF(NOW(),roomlogs.enterTime) = 0 and roomlogs.netId=user_pic.netId AND roomlogs.`status` = 0 and roomlogs.roomId = proom and userprofile.netId = roomlogs.netId  and roomlogs.hidden!=1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_student_number_total_and_checkedin
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_student_number_total_and_checkedin`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_student_number_total_and_checkedin`(pCourseNumber varchar(100),proom varchar(200))
begin
	declare total,checkedin int default 0;
	select count(*) into total from rosters where courseNumber=pCourseNumber;
	select count(*) into checkedin from roomlogs where DATEDIFF(NOW(),roomlogs.enterTime) = 0 and roomlogs.`status` = 0 and roomlogs.roomId = proom and roomlogs.hidden!=1;
	select total,checkedin;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_today_events
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_today_events`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_today_events`()
begin 
		select events.eventId,events.date,TIMESTAMPDIFF(day,events.date,NOW()) from calendar,events where calendar.tag = events.eventId;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_today_events2
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_today_events2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_today_events2`()
begin 
		select events.* from events where TIMESTAMPDIFF(hour,events.date,NOW()) < 24 and TIMESTAMPDIFF(hour,events.date,NOW()) > 0;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_today_student_in_room
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_today_student_in_room`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_today_student_in_room`(proomid varchar(50), ptime timestamp)
begin
	select roomlogs.*,users.email from roomlogs,users where DATEDIFF(roomlogs.enterTime,ptime) = 0 and roomlogs.roomId = proomid
and roomlogs.netId = users.netid and roomlogs.hidden !=1;

	#select roomlogs.*,users.email from roomlogs,users where roomlogs.roomId = 'flag' and DATEDIFF(roomlogs.enterTime,ptime) = 0 and roomlogs.roomId = proomid
#and roomlogs.netId = users.netid and roomlogs.hidden !=1;


end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_uncheckin_users
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_uncheckin_users`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_uncheckin_users`(proom varchar(200),pCourseNumber varchar(100))
begin
	select userprofile.*,IFNULL(user_pic.picName,'') as 'picName' from userprofile left join user_pic on user_pic.netId=userprofile.netId where userprofile.netId in 
		(
			select 
				rosters.userName 
			from 
				rosters 
			where rosters.userName not in
				(select roomlogs.netId from roomlogs,userprofile where DATEDIFF(NOW(),roomlogs.enterTime) = 0 and roomlogs.status = 0 and roomlogs.roomId = proom and roomlogs.hidden!=1)
			and
				rosters.courseNumber = pCourseNumber
		);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_unlocate_address
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_unlocate_address`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_unlocate_address`(IN startid int)
begin

	select locations.id,locations.address as name from locations where id > startid and locations.status = 0 limit 0,15;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_user_event
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_user_event`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_user_event`(pstartdate datetime, penddate datetime)
begin
	select * from userevents where TIMESTAMPDIFF(day,userevents.date,pstartdate)=0 or TIMESTAMPDIFF(day,userevents.date,penddate)=0 or 
    (TIMESTAMPDIFF(day,userevents.date,pstartdate)<0 and TIMESTAMPDIFF(day,userevents.date,penddate)>0);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for get_user_profile
-- ----------------------------
DROP PROCEDURE IF EXISTS `get_user_profile`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_user_profile`(pnetid varchar(50))
BEGIN

	select * from userprofile where userprofile.netId = pnetid;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for leader_board
-- ----------------------------
DROP PROCEDURE IF EXISTS `leader_board`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `leader_board`(proomId varchar(200))
begin

   select count(*) as total from roomlogs where roomId = proomId and roomlogs.`status` = 0 and DATEDIFF(roomlogs.enterTime,NOW()) = 0 and roomlogs.hidden!=1;

end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for manually_check_in
-- ----------------------------
DROP PROCEDURE IF EXISTS `manually_check_in`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `manually_check_in`(pnet varchar(50), proom varchar(50), penter timestamp, pdur bigint, pcode int, plat double, plng double)
begin
	insert into roomcheckin (netId,roomId,enterTime,duration,code,lat,lng)value(pnet,proom,penter,pdur,pcode,plat,plng);
	select * from roomcheckin where id = LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for manually_check_inV2
-- ----------------------------
DROP PROCEDURE IF EXISTS `manually_check_inV2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `manually_check_inV2`(pnet varchar(50), proom varchar(50), penter timestamp, pdur bigint, pcode int, plat double, plng double, paltit double, psource varchar(10))
begin
	insert into roomcheckin (netId,roomId,enterTime,duration,code,lat,lng,altit,roomcheckin.locationSource)value(pnet,proom,penter,pdur,pcode,plat,plng,paltit,psource);
	select * from roomcheckin where id = LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for manually_check_inV3
-- ----------------------------
DROP PROCEDURE IF EXISTS `manually_check_inV3`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `manually_check_inV3`(pnet varchar(50), proom varchar(50), penter timestamp, pdur bigint, pcode int, plat double, plng double, paltit double, psource varchar(10),pBy varchar(50))
begin
	insert into roomcheckin (netId,roomId,enterTime,duration,code,lat,lng,altit,roomcheckin.locationSource,roomcheckin.`by`)value(pnet,proom,penter,pdur,pcode,plat,plng,paltit,psource,pBy);
	select * from roomcheckin where id = LAST_INSERT_ID();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for mark_pushed_events
-- ----------------------------
DROP PROCEDURE IF EXISTS `mark_pushed_events`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `mark_pushed_events`(peventid int)
begin
	insert into event_pushed_marks (eventid)value(peventid);
	select row_count();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for new_leader_board_function
-- ----------------------------
DROP PROCEDURE IF EXISTS `new_leader_board_function`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `new_leader_board_function`()
begin

select  
(select count(distinct roomlogs.netId) from roomlogs where  roomlogs.hidden!=1 and roomlogs.roomId = rs.roomId and DATEDIFF(NOW(),roomlogs.enterTime) < 8) as countOfPast,
(select count(*) from roomlogs where  roomlogs.hidden!=1 and roomlogs.roomId = rs.roomId and roomlogs.`status` = 0 and DATEDIFF(roomlogs.enterTime,NOW()) = 0) as countOfNow,
rs.building,rs.roomId,rs.roomname,br.beaconNumber,bv.UUID,bv.major,bv.minor from rooms rs,beaconRoomRelations br,beaconv2 bv 
where rs.active = 1 and br.beaconNumber = bv.beaconNumber  and rs.roomId = br.roomId;

#select  
#(select 0) as countOfPast,
#(select 0) as countOfNow,
#rs.building,rs.roomId,rs.roomname,br.beaconNumber,bv.UUID,bv.major,bv.minor from rooms rs,beaconRoomRelations br,beaconv2 bv 
#where rs.active = 1 and br.beaconNumber = bv.beaconNumber  and rs.roomId = br.roomId;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for question_set_1
-- ----------------------------
DROP PROCEDURE IF EXISTS `question_set_1`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `question_set_1`(pcourseNumber varchar(50))
    DETERMINISTIC
begin

declare p_roomId varchar(50);
declare p_startTime varchar(10);
declare p_endTime varchar(10);

select coursev2.roomId INTO p_roomId from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
select coursev2.startTime into p_startTime from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
select coursev2.endTime into p_endTime from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
######################BEGIN ############################

#########    ANSWER FOR User's Platfrom  ######### 
DROP TABLE IF EXISTS `data_analysis_temp2`;
CREATE TABLE data_analysis_temp2 SELECT * from((
select Q.*,

#########    IF users turn off Location sharing by Qunfang #########
#########  TODO: find out the corresponding labels for android app @Sunyu ### 
######### first set up,  @Qunfang, please split the counts to two (first setup/later reminder)
(select COUNT(*) from records where records.device = Q.device and 
	(records.`event` = 'TapCancelOnFirstLocationAlert' or records.`event` = 'TapCancelOnFirstNavigationAlert' 
	OR (records.event='PermissionLocationServiceTurned' and records.data like'{
  "Indicator" : "0",%'))) as CountOfCancelLocation,

#########    ANSWER FOR BLUETOOTH @Sun Yu: this should only be about bluetooth behavior ######
(select COUNT(*) from records where records.device = Q.device and (records.`event` = 'Bluetooth Turned Off' OR  records.`event` = 'Broadcast OFF' OR records.`event` = 'stopSharingPressed'
					or records.`event` = 'Bluetooth OFF'or (records.event='PermissionBluetoothTurned'and records.data like'{
  "Indicator" : "0",%'))) as CountOfBluetoothOff,

######### @Sun Yu, please confirm what broadcast off means in Android ######### 
##### WE remove this line (select COUNT(*) from records where records.device = Q.device and records.`event` = 'TapCancelOnFirstBLEAlert') as CountOfTapCancelOnFirstBLEAlert,
(select COUNT(*) from records where records.device = Q.device and (records.`event` = 'Broadcast OFF' OR records.`event` = 'stopSharingPressed')) as CountOfBroadcastOFF,

#########    ANSWER FOR 'TabSocialTabButton'  ######### 
#########    @Qunfang, split this to two numbers, in class/outside the class 
(select COUNT(*) from records where records.device = Q.device and records.`event` = 'TabSocialTabButton') as CountOfTabSocialTabButton,

#########    @Qunfang: to add a query to count if people click "NOW"   ######### 
#########    @SunYu: to add a query to count if people click "NOW"   ######### 
(select COUNT(*) from records where records.device = Q.device and records.`event` = 'FriendTvCellPressed') as CountOfNowButtonPressed,

#########    @Qunfang: to add a query to count if people click "Past"   ######### 
#########    @SunYu: to add a query to count if people click "Past"   ######### 
(select COUNT(*) from records where records.device = Q.device and records.`event` = 'PastButtonPressed') as CountOfPastButtonPressed,

#########    ANSWER FOR 'TapEventListTabButton'  ######### 
(select COUNT(*) from records where records.device = Q.device and records.`event` = 'TapEventListTabButton') as CountOfTapEventListTabButton,

#########    ANSWER FOR Detected By Other Beacons  ######### 
(select count(DISTINCT roomlogs.roomId,DATE(roomlogs.enterTime)) from roomlogs where roomlogs.netId = Q.netid and (roomlogs.roomId != p_roomId)) 
 as countOfDetectedByOtherBeacons,

#########    ANSWER FOR Showing up times.including students who come earlier than the class starts ######### 
#########    @Sun Yu, add the condition that includes who came earlier but leave later. 
(
select count(DISTINCT(DATE(roomlogs.enterTime))) from roomlogs
 where roomlogs.netId = Q.netid 
and
	((time(roomlogs.enterTime) > p_startTime and time(roomlogs.enterTime) < p_endTime) or  
		(time(roomlogs.exitTime) > p_startTime and time(roomlogs.exitTime) < p_endTime)) 
and
	(DAYOFWEEK(roomlogs.enterTime) = 2 or (DAYOFWEEK(roomlogs.enterTime) = 4 and pcourseNumber !='21380.1181m') )

)as showsup,

#########    ANSWER FOR beacon-based NOTIFICATION######### 
##########   @Sun Yu: please check if android has beacon-based notifications. 
(select COUNT(*) from records where records.device = Q.device and records.`event` = 'AppInitiatedByNotification') as CountOfAppInitiatedByNotification

######################END ############################

from(select a.netid, a.platform, ro.courseNumber,r.device
from records r
inner join apikeys a
inner join rosters ro
on  r.device = a.device and concat(ro.username, '@syr.edu') =a.email and courseNumber Like pcourseNumber
GROUP BY a.netid, ro.courseNumber, a.platform 
ORDER BY ro.courseNumber) Q)QRes);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for question_set_1_V2
-- ----------------------------
DROP PROCEDURE IF EXISTS `question_set_1_V2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `question_set_1_V2`(IN `pcourseNumber` varchar(50))
    DETERMINISTIC
begin

declare p_roomId varchar(50);
declare p_startTime varchar(10);
declare p_endTime varchar(10);

select coursev2.roomId INTO p_roomId from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
select coursev2.startTime into p_startTime from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
select coursev2.endTime into p_endTime from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
######################BEGIN ############################

#########    ANSWER FOR User's Platfrom  ######### 
DROP TABLE IF EXISTS `data_analysis_result_v3`;
CREATE TABLE data_analysis_result_v3 SELECT * from((
select Q.*,

#########    IF users turn off Location sharing by Qunfang #########
#########  TODO: find out the corresponding labels for android app @Sunyu ### 
######### first set up,  @Qunfang, please split the counts to two (first setup/later reminder)
####First setup
(select COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and 
	(records.`event` = 'TapCancelOnFirstLocationAlert' or records.`event` = 'TapCancelOnFirstNavigationAlert')) as CountOfFirstCancelLocation,
####Later reminder
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and 
	((records.event='PermissionLocationServiceTurned' and records.data like'{
  "Indicator" : "0",%'))) as CountOfLaterCancelLocation,

#########    ANSWER FOR BLUETOOTH @Sun Yu: this should only be about bluetooth behavior ###### 'Bluetooth OFF' for Android
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and (records.`event` = 'Bluetooth Turned Off' or records.`event` = 'Bluetooth OFF'or (records.event='PermissionBluetoothTurned'and records.data like'{
  "Indicator" : "0",%'))) as CountOfBluetoothOff,

######### @Sun Yu, please confirm what broadcast off means in Android ######### 'Broadcast OFF' for Android and 'stopSharingPressed' for iOS
##### WE remove this line (select COUNT(*) from records where records.device = Q.device and records.`event` = 'TapCancelOnFirstBLEAlert') as CountOfTapCancelOnFirstBLEAlert,
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and (records.`event` = 'Broadcast OFF' OR records.`event` = 'stopSharingPressed')) as CountOfBroadcastOFF,

#########    ANSWER FOR 'TabSocialTabButton'  ######### 
#########    @Qunfang, split this to two numbers, in class/outside the class 
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'TabSocialTabButton') as CountOfTabSocialTabButton,
###In class
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'TabSocialTabButton'and 
((time(records.createTime) between p_startTime and p_endTime)) 
and
	(DAYOFWEEK(records.createTime) = 2 or (DAYOFWEEK(records.createTime) = 4 and pcourseNumber !='21380.1181m'))
) as CountOfTabSocialTabButtonInClass,
###Outside the class
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'TabSocialTabButton'and 
not ((time(records.createTime) between p_startTime and p_endTime)
and
	(DAYOFWEEK(records.createTime) = 2 or (DAYOFWEEK(records.createTime) = 4 and pcourseNumber !='21380.1181m')))
) as CountOfTabSocialTabButtonOutClass,
#########    @Qunfang: to add a query to count if people click "NOW"   ######### 
#########    @SunYu: to add a query to count if people click "NOW"   ######### 
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'FriendTvCellPressed') as CountOfNowButtonPressed,
###In class
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'FriendTvCellPressed' and
((time(records.createTime) between p_startTime and p_endTime)) 
and
	(DAYOFWEEK(records.createTime) = 2 or (DAYOFWEEK(records.createTime) = 4 and pcourseNumber !='21380.1181m'))
) as CountOfNowButtonPressedInClass,
###Outside the class
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'FriendTvCellPressed'and 
not ((time(records.createTime) between p_startTime and p_endTime)
and
	(DAYOFWEEK(records.createTime) = 2 or (DAYOFWEEK(records.createTime) = 4 and pcourseNumber !='21380.1181m')))
) as CountOfNowButtonPressedOutClass,

#########    @Qunfang: to add a query to count if people click "Past"   ######### 
#########    @SunYu: to add a query to count if people click "Past"   ######### 
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'PastButtonPressed') as CountOfPastButtonPressed,
###In class
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'PastButtonPressed'and
((time(records.createTime) between p_startTime and p_endTime)) 
and
	(DAYOFWEEK(records.createTime) = 2 or (DAYOFWEEK(records.createTime) = 4 and pcourseNumber !='21380.1181m'))
) as CountOfPastButtonPressedInClass,
###Outside the class
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'PastButtonPressed'and 
not ((time(records.createTime) between p_startTime and p_endTime)
and
	(DAYOFWEEK(records.createTime) = 2 or (DAYOFWEEK(records.createTime) = 4 and pcourseNumber !='21380.1181m')))
) as CountOfPastButtonPressedOutClass,
#########only android app has this record
(select count(DISTINCT(records.createTime)) from records where records.`event` = 'Student profile selected from ROOM' and records.device = Q.device) as TapOtherUserProfile,
#########    ANSWER FOR 'TapEventListTabButton'  ######### 
(select  COUNT(DISTINCT(DATE(records.createTime))) from records where records.device = Q.device and records.`event` = 'TapEventListTabButton') as CountOfTapEventListTabButton,
#########CheckEventDetails from map or event list
(select count(DISTINCT(records.createTime)) from records where records.`event` = 'TapEventCellOnEventScreen' and records.device = Q.device) as CheckEventDetailsFromMap,
(select count(DISTINCT(records.createTime)) from records where records.`event` = 'TapEventDetailView' and records.device = Q.device) as CheckEventDetailsFromList,
#########    ANSWER FOR Detected By Other Beacons  ######### 
(select count(DISTINCT roomlogs.roomId,DATE(roomlogs.enterTime)) from roomlogs where roomlogs.netId = Q.netid and (roomlogs.roomId != p_roomId)) 
 as countOfDetectedByOtherBeacons,

#########    ANSWER FOR Showing up times.including students who come earlier than the class starts ######### 
#########    @Sun Yu, add the condition that includes who came earlier but leave later. 
(
select count(DISTINCT(DATE(roomlogs.enterTime))) from roomlogs
 where roomlogs.netId = Q.netid 
and
	(((time(roomlogs.enterTime) > p_startTime and time(roomlogs.enterTime) < p_endTime) or  
		(time(roomlogs.exitTime) > p_startTime and time(roomlogs.exitTime) < p_endTime)) OR
	(time(roomlogs.enterTime) < p_startTime and time(roomlogs.exitTime) > p_endTime))
	
and
	(DAYOFWEEK(roomlogs.enterTime) = 2 or (DAYOFWEEK(roomlogs.enterTime) = 4 and pcourseNumber !='21380.1181m')) 

)as showsup,

(
select count(DISTINCT(DATE(roomlogs.enterTime))) from roomlogs
 where roomlogs.netId = Q.netid 
and
	(((time(roomlogs.enterTime) > p_startTime and time(roomlogs.enterTime) < p_endTime) or  
		(time(roomlogs.exitTime) > p_startTime and time(roomlogs.exitTime) < p_endTime)) OR
	(time(roomlogs.enterTime) < p_startTime and time(roomlogs.exitTime) > p_endTime))
	
and
	(DAYOFWEEK(roomlogs.enterTime) = 2 or (DAYOFWEEK(roomlogs.enterTime) = 4 and pcourseNumber !='21380.1181m')) 

and roomlogs.checkinId > 0

)as manuallyCheckIn,

(select(showsup - manuallyCheckIn))as autoCheckIn,

###########first time Installed the app
(select records.createTime from records where records.device = Q.device  limit 0,1) as  firstlyInstalled,

#########    ANSWER FOR beacon-based NOTIFICATION######### 
##########   @Sun Yu: please check if android has beacon-based notifications. -- No.
(select COUNT(*) from records where records.device = Q.device and records.`event` = 'AppInitiatedByNotification') as CountOfAppInitiatedByNotification

######################END ############################

from(select a.netid, a.platform, ro.courseNumber,r.device
from records r
inner join apikeys a
inner join rosters ro
on  r.device = a.device and concat(ro.username, '@syr.edu') =a.email and courseNumber Like pcourseNumber
GROUP BY a.netid,a.device,ro.courseNumber, a.platform 
ORDER BY ro.courseNumber) Q)QRes);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for question_set_2
-- ----------------------------
DROP PROCEDURE IF EXISTS `question_set_2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `question_set_2`(pcourseNumber varchar(50), pbegin varchar(10), pend varchar(10))
    DETERMINISTIC
begin

declare p_roomId varchar(50);
declare p_startTime varchar(10);
declare p_endTime varchar(10);

select coursev2.roomId INTO p_roomId from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
select coursev2.startTime into p_startTime from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
select coursev2.endTime into p_endTime from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
######################BEGIN ############################
DROP TABLE IF EXISTS `data_analysis_message_EC2`;
CREATE TABLE data_analysis_message_EC2
SELECT * from((
select Q.*,
(select roomlogs.enterTime from roomlogs where roomlogs.netId = Q.netid order by roomlogs.roomlogId asc limit 0,1 ) as firstTimeDetected,

#######################Count of sending message#########################################
(select count(*) from messages where messages.sender = Q.netid) as countOfSendEmail,
#######################Count of strangers of receiver #########################################
(select count(*) from messages where messages.sender = Q.netid and messages.receiver!=messages.sender and (messages.receiver 
not in (select rosters.userName from rosters where rosters.courseNumber = pcourseNumber))) as receiverIsStranger,

#######################Open App times beyond the class time #########################################
####################### @Sun Yu, only include the events that contain '%Tap%' or '%Tab%' or '%App%'
####################### @Sun Yu, exclude the classtime using the first query you wrote, two parameters, class id and grace period (in minutes) 
(select count(DISTINCT(Date(records.createTime))) from records where  records.device = Q.device 
and (time(records.createTime) > pbegin or  time(records.createTime) < pend)) as openAppTimes
######################END ############################

from(select a.netid, a.platform, ro.courseNumber,r.device
from records r
inner join apikeys a
inner join rosters ro
on  r.device = a.device and concat(ro.username, '@syr.edu') =a.email and courseNumber Like pcourseNumber
GROUP BY a.netid, a.platform, ro.courseNumber 
ORDER BY ro.courseNumber) Q)QRes);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for question_set_2_v2
-- ----------------------------
DROP PROCEDURE IF EXISTS `question_set_2_v2`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `question_set_2_v2`(pcourseNumber varchar(50))
begin

declare p_roomId varchar(50);
declare p_startTime varchar(10);
declare p_endTime varchar(10);

select coursev2.roomId INTO p_roomId from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
select DATE_ADD(STR_TO_DATE(coursev2.startTime,'%H:%i'),INTERVAL 60*-1 MINUTE)  into p_startTime from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
select DATE_ADD(STR_TO_DATE(coursev2.endTime,'%H:%i'),INTERVAL 60 MINUTE) into p_endTime from coursev2 WHERE coursev2.courseNumber = pcourseNumber;
######################BEGIN ############################
DROP TABLE IF EXISTS `data_analysis_result2_v3`;
CREATE TABLE data_analysis_result2_v3
SELECT * from((
select Q.*,
(select roomlogs.enterTime from roomlogs where roomlogs.netId = Q.netid order by roomlogs.roomlogId asc limit 0,1 ) as firstTimeDetected,

#######################Count of sending message#########################################
(select count(*) from messages where messages.sender = Q.netid) as countOfSendEmail,
#######################Count of strangers of receiver #########################################
(select count(*) from messages where messages.sender = Q.netid and messages.receiver!=messages.sender and (messages.receiver 
not in (select rosters.userName from rosters where rosters.courseNumber = pcourseNumber))) as receiverIsStranger,

#######################Open App times beyond the class time #########################################
####################### @Sun Yu, only include the events that contain '%Tap%' or '%Tab%' or '%App%'
####################### @Sun Yu, exclude the classtime using the first query you wrote, two parameters, class id and grace period (in minutes) 
(select count(DISTINCT(Date(records.createTime))) from records where  records.device = Q.device 
and 
###########time condition
	((((DAYOFWEEK(records.createTime) = 2 or (DAYOFWEEK(records.createTime) = 4 and pcourseNumber !='21380.1181m'))) 
	 and (time(records.createTime) < p_startTime or time(records.createTime) > p_endTime))
	or 
	
	DAYOFWEEK(records.createTime) = 1 or DAYOFWEEK(records.createTime) = 3 or DAYOFWEEK(records.createTime) = 5 or DAYOFWEEK(records.createTime) = 6)
###########

) as openAppTimes
######################END ############################

from(select a.netid, a.platform, ro.courseNumber,r.device
from (select * from records where records.`event` like '%Tap%' or records.`event` like '%Tab%' or records.`event` like '%App%' or records.`event` like '%press%') as r
inner join apikeys a
inner join rosters ro
on  r.device = a.device and concat(ro.username, '@syr.edu') =a.email and courseNumber Like pcourseNumber
GROUP BY a.netid,a.device, a.platform, ro.courseNumber 
ORDER BY ro.courseNumber) Q)QRes);
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for room_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `room_list`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `room_list`()
begin
select rooms.lat,rooms.lng,rooms.`code`,rooms.building,rooms.roomId,rooms.roomname,beaconRoomRelations.beaconNumber,beaconv2.UUID,beaconv2.major,beaconv2.minor from rooms,beaconRoomRelations,beaconv2 where beaconRoomRelations.beaconNumber = beaconv2.beaconNumber  and rooms.roomId = beaconRoomRelations.roomId and rooms.active = 1;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for search_cache_events_by_keyword
-- ----------------------------
DROP PROCEDURE IF EXISTS `search_cache_events_by_keyword`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `search_cache_events_by_keyword`(IN `pcondition` varchar(100), IN `pdate` bigint, IN `ptable` varchar(50), IN `pstartId` int)
begin
			set @s = concat('select 
					 * 
			from 
					',ptable,'  tb where where status = 1 and (UPPER(tb.title) LIKE UPPER(''%',pcondition,'%'') 

			or UPPER(tb.desc LIKE ''%',pcondition,'%'')) 
			and (dateinlong =',pdate,' or dateinlong >',pdate,') order by dateinlong limit 0,60');

			prepare stmt from @s;
			EXECUTE stmt;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for search_cache_events_by_key_and_categories
-- ----------------------------
DROP PROCEDURE IF EXISTS `search_cache_events_by_key_and_categories`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `search_cache_events_by_key_and_categories`(IN `plist` varchar(200), IN `pkey` varchar(100), IN `pdate` bigint, IN `ptable` varchar(50), IN `pstartId` int)
begin

     
			set @s = concat('select 
					* 
			from 
					',ptable,' tb where (UPPER(tb.title) LIKE  UPPER(''%',pkey,'%'') 

			or UPPER(tb.desc) LIKE  UPPER(''%',pkey,'%'')) and (dateinlong = ',pdate,' or dateinlong > ',pdate,') and 
			category in (',plist,') and status = 1
			 limit 0,20');
			prepare stmt from @s;
			EXECUTE stmt;


end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for search_cache_event_by_categories
-- ----------------------------
DROP PROCEDURE IF EXISTS `search_cache_event_by_categories`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `search_cache_event_by_categories`(IN `pcategories` varchar(200), IN `pdate` bigint, IN `ptable` varchar(20), IN `pstartId` int)
    DETERMINISTIC
begin
			set @s = concat(
			'select * from ',ptable,' where status = 1 and category in (',pcategories,') and (dateinlong = ',pdate,' or dateinlong > ',pdate,')
			 order by dateinlong limit 0,60');
			prepare stmt from @s;
			EXECUTE stmt;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for student_attendance
-- ----------------------------
DROP PROCEDURE IF EXISTS `student_attendance`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `student_attendance`(pnetId varchar(50), pcourseId varchar(50))
    DETERMINISTIC
begin
	declare daysstr varchar(20);
	
	declare rId varchar(20);
	declare startTime_ varchar(20);
	declare endTime_ varchar(20);

	declare semester_start varchar(20);
	declare semester_end varchar(20);

	

	select courseV2.roomId into rId from courseV2 where courseNumber = pcourseId;
	select courseV2.days into daysstr from courseV2 where courseV2.courseNumber = pcourseId;
	select courseV2.startTime into startTime_ from courseV2 where courseV2.courseNumber = pcourseId;
	select courseV2.endTime into endTime_ from courseV2 where courseV2.courseNumber = pcourseId;
	select semester.startDate into semester_start from semester limit 0,1;
	select semester.endDate into semester_end from semester limit 0,1;
	
	
	select DATE_FORMAT(result2.enterTimeWithDate,'%Y-%m-%d %k:%i') as enterTimeWithDate,result2.enterTime as enterTime,result2.userName,result2.checkinId,result2.codes,result3.firstName,result3.lastName,result3.courseNumber from (select result.enterTime as enterTimeWithDate,result.roomId,result.checkinId,result.netId as userName,DATE_FORMAT(result.enterTime,'%k:%i') as enterTime,roomcheckin.code as codes  from (select *
	from roomlogs where (roomlogs.netId = pnetId and roomlogs.roomId = rId) and roomlogs.enterTime > semester_start and  roomlogs.enterTime < semester_end and
	find_in_set((DAYOFWEEK(roomlogs.enterTime) - 1),daysstr)>0 and DATE_FORMAT(DATE_ADD(roomlogs.enterTime, INTERVAL 11 MINUTE),'%k:%i') > startTime_ and DATE_FORMAT(DATE_ADD(roomlogs.enterTime, INTERVAL 11 MINUTE),'%k:%i') < endTime_ group by DATE_FORMAT(roomlogs.enterTime,'%y-%m-%j')) 
	result left join roomcheckin on result.checkinId = roomcheckin.id) result2, (select * from rosters where rosters.userName = pnetId and rosters.courseNumber = pcourseId limit 0 ,1) result3 WHERE
	result2.userName = result3.userName order by  result2.enterTimeWithDate desc;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for ttt
-- ----------------------------
DROP PROCEDURE IF EXISTS `ttt`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ttt`(pbegin datetime, pend datetime, pcn varchar(20))
begin
	select *,
	(select roomlogs.enterTime from roomlogs where (roomlogs.enterTime > pbegin and roomlogs.enterTime < pend)  and roomlogs.roomId = roster_course.roomId and roomlogs.netId = roster_course.userName order by roomlogs.enterTime limit 0,1) as enterTime 

	from (select rosters.*,coursev2.roomId from rosters,coursev2 where rosters.courseNumber = pcn and rosters.courseNumber = coursev2.courseNumber)roster_course;
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for update_course_schedule_track
-- ----------------------------
DROP PROCEDURE IF EXISTS `update_course_schedule_track`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `update_course_schedule_track`(pnetId varchar(50))
begin
	update courseScheduleTrack set lastAccess = now() where courseScheduleTrack.netId = pnetId;
	select ROW_COUNT();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for update_event_status
-- ----------------------------
DROP PROCEDURE IF EXISTS `update_event_status`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `update_event_status`()
begin
	update `events` e set e.`status` = 1 where type = 'official';
	select ROW_COUNT();
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for update_location
-- ----------------------------
DROP PROCEDURE IF EXISTS `update_location`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `update_location`(IN plocation varchar(200), IN plat double, IN plng double, IN plevel varchar(50), IN poffice varchar(50), IN pcategory varchar(50), IN palt double, IN pType varchar(10), palias varchar(100))
begin
	declare row_ int;
	declare result_ int;
	
	select count(*) into row_ from locations where locations.address = plocation;
	
	if row_ = 0 then
		delete from locations where locations.address = palias;
		insert into locations 
			(locations.address,locations.lat,locations.lng,locations.status,locations.roomNo,locations.floor,locations.category,
		locations.alt,locations.locationType,locations.alias)
			values
			(plocation,plat,plng,1,poffice,plevel,pcategory,palt,pType,palias);
		SELECT LAST_INSERT_ID() as result;
	else
		delete from locations where locations.address = palias;
		update locations set locations.lat = plat,locations.lng = plng,locations.status = 1, locations.floor = plevel,
		locations.roomNo = poffice,locations.category = pcategory,locations.alt = palt, locations.locationType = pType
		,locations.alias = palias where locations.address = plocation;
		select locations.id from locations where locations.address = plocation limit 0,1;
	end if;
	
	
	
end;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for update_non_exit_log
-- ----------------------------
DROP PROCEDURE IF EXISTS `update_non_exit_log`;
delimiter ;;
CREATE DEFINER=`root`@`%` PROCEDURE `update_non_exit_log`()
begin
	update roomlogs set `status` = 1 where roomlogId in (select * from (select rl.roomlogId from roomlogs rl,rooms where rl.roomId = rooms.roomId and  rl.`status` = 0 and TIMESTAMPDIFF(MINUTE,rl.enterTime,NOW()) > rooms.threshold) as tt);
	select ROW_COUNT();
end;
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
