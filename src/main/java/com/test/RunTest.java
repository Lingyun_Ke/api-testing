package com.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.lang.reflect.Field;
import java.net.ConnectException;
import java.util.*;

public class RunTest {
    public static void main(String[] args) {
        try {
            Class clazz = Class.forName("com.test.URLs");
            Map<String,Map<String,String>> paramsMap = URLs.paramsMap;
            Field[] fields = clazz.getFields();
            int numPassed = 0;
            int numFailed = 0;
            for(int i=0;i<fields.length;i++){
                Field f = fields[i];
                String name = f.getName();
                if(name.startsWith("url")){
                    String urlValue = f.get(clazz).toString();
                    Map<String,String> params = paramsMap.get(name);
                    try {
                        int result=0;
                        if(urlValue.contains("addOrUpdateUserSelfPic")||urlValue.contains("uploadPhotos")){
                            result = uploadFileByHTTP(urlValue,params);
                        } else{
                            result = sendPost(urlValue,params);
                        }
                        if(result==1){
                            numPassed++;
                        } else {
                            numFailed++;
                        }
                    } catch (ConnectException e){
                        numFailed++;
                        System.out.println("URL "+urlValue+" encounters connection failed");
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            System.out.println("\n\n\nTEST FINISHED! TOTAL NUMBER OF TESTED URLS: "+(numFailed+numPassed)+
                    ", NUMBER OF FAILED URLS:"+numFailed);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static int sendPost(String url, Map<String, String> map) throws Exception{
        CloseableHttpClient httpclient = HttpClients.createDefault();
        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
        if(map==null) map = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
        HttpPost httppost = new HttpPost(url);
        httppost.setEntity(entity);
        CloseableHttpResponse response = httpclient.execute(httppost);
        HttpEntity resEntity = response.getEntity();
        String result = EntityUtils.toString(resEntity);
        JSONObject json = JSON.parseObject(result);

        if(response.getStatusLine().getStatusCode()==200&&("1".equalsIgnoreCase(json.get("code").toString())||"success".equalsIgnoreCase(json.get("message").toString()))){
            System.out.println("The URL "+url+" pass the test with the return message \"SUCCESS\"");
            return 1;
        } else if (response.getStatusLine().getStatusCode()==200){
            System.out.print("The URL seems didn't pass the test with the error message "+json.get("message"));
            return -1;
        } else{
            System.out.print("The API "+url+" connection error with status code "+response.getStatusLine().getStatusCode());
            return -1;
        }
    }

    public static int uploadFileByHTTP(String url, Map<String,String> map) throws Exception{
        File postFile = new File(map.get("filepath"));
        map.remove("filepath");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        FileBody fundFileBin = new FileBody(postFile);
        MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
        multipartEntity.addPart(postFile.getName(), fundFileBin);
        Set<String> keySet = map.keySet();
        for (String key : keySet) {
            multipartEntity.addPart(key, new StringBody(map.get(key), ContentType.create("text/plain", Consts.UTF_8)));
        }

        HttpEntity reqEntity = multipartEntity.build();
        httpPost.setEntity(reqEntity);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity resEntity = response.getEntity();
        String result = EntityUtils.toString(resEntity);
        JSONObject json = JSON.parseObject(result);
        if(response.getStatusLine().getStatusCode()==200&&("1".equalsIgnoreCase(json.get("code").toString())||"success".equalsIgnoreCase(json.get("message").toString()))){
            System.out.println("The URL "+url+" pass the test with the return message \"SUCCESS\"");
            return 1;
        } else if (response.getStatusLine().getStatusCode()==200){
            System.out.print("The URL seems didn't pass the test with the error message "+json.get("message"));
            return -1;
        } else{
            System.out.print("The API "+url+" connection error with status code "+response.getStatusLine().getStatusCode());
            return -1;
        }
    }

}
