package com.test;

import java.util.HashMap;
import java.util.Map;

public class URLs {

    //declare all the request urls
    static final public String serverPrefix = "http://localhost:8080";
    static final public String url1 = serverPrefix+"/control/events.do?method=addEvent";
    static final public String url2 = serverPrefix+"/control/events.do?method=clearBeaconCache";
    static final public String url3 = serverPrefix+"/control/events.do?method=getBeaconPushNotification";
    static final public String url4 = serverPrefix+"/control/events.do?method=getEvents";
    static final public String url5 = serverPrefix+"/control/events.do?method=getEventsOfCalendar";
    static final public String url6 = serverPrefix+"/control/events.do?method=getOfficialAndUserEvents";
    static final public String url7 = serverPrefix+"/control/events.do?method=saveToCalendar";
    static final public String url8 = serverPrefix+"/control/login.do?method=registerDevice";
    static final public String url9 = serverPrefix+"/control/logservice.do?method=uploadLog";
    static final public String url10 = serverPrefix+"/control/photoV2.do?method=getAlbum";
    static final public String url11 = serverPrefix+"/control/photoV2.do?method=getPhotoList";
    static final public String url12 = serverPrefix+"/control/photoV2.do?method=uploadPhotos";
    static final public String url13 = serverPrefix+"/control/room.do?method=checkInRoomWithCode";
    static final public String url14 = serverPrefix+"/control/room.do?method=enter";
    static final public String url15 = serverPrefix+"/control/room.do?method=exit";
    static final public String url16 = serverPrefix+"/control/room.do?method=getCourseScheduleV2";
    static final public String url17 = serverPrefix+"/control/room.do?method=getPastUserInRoom";
    static final public String url18 = serverPrefix+"/control/room.do?method=getRoomId";
    static final public String url19 = serverPrefix+"/control/room.do?method=getRoomInfo";
    static final public String url20 = serverPrefix+"/control/room.do?method=leaderBoardV2";
    static final public String url21 = serverPrefix+"/control/room.do?method=roomlist";
    static final public String url22 = serverPrefix+"/control/user.do?method=getPicNameByNetId";
    static final public String url23 = serverPrefix+"/control/search.do?method=search";
    static final public String url24 = serverPrefix+"/control/system.do?method=getVersion";
    static final public String url25 = serverPrefix+"/control/user.do?method=addOrUpdateUserProfile";
    static final public String url26 = serverPrefix+"/control/user.do?method=saveUserPreferences";
    static final public String url27 = serverPrefix+"/control/user.do?method=userProfile";
    static final public String url28 = serverPrefix+"/control/user.do?method=addOrUpdateUserSelfPic";

    //store all the urls' params in this map
    static final public Map<String,Map<String,String>> paramsMap = new HashMap<String, Map<String, String>>();

    //add request params here
    static {
        paramsMap.put("url1",new HashMap<String, String>(){{
            put("parameters","{\"event\":{\"category\":\"Academic\",\"Date\":\"2018-08-10\",\"capacity\":\"20\",\"Desc\":\"event desc\"" +
                    ",\"email\":\"test@test.com\",\"eventId\":\"123\",\"Lat\":37.232323,\"Lng\":-122.0434,\"Location\":\"event's location\"" +
                    ",\"Phone\":\"301-332-4221\",\"Timebegin\":\"3:00\",\"Timeend\":\"4:00\",\"title\":\"event title\",\"userId\":\"test\"}}");
        }});
        paramsMap.put("url2",new HashMap<String, String>(){{
            put("email","test@test.com");
        }});
        paramsMap.put("url3",new HashMap<String, String>(){{
            put("parameters","{ \"beacon\": { \"major\":\"2230\", \"minor\":\"54528\", \"uuid\":\"B9407F30-F5F8-466E-AFF9-25556B57FE6D\" }, \"device\":\"device id\", \"user\":\"user email\" }");
        }});
        paramsMap.put("url4",new HashMap<String, String>(){{
            put("parameters","{ \"categories\":[ \"Athletics\", \"Career Events\", \"Religious\", \"Social\", \"Speakers\", \"Student Organizations\", \"Sustainability\", \"Other\" ], \"endDate\":1470716885192, \"keyword\":\"strawberry festive\", \"searching\":false, \"startDate\":1469507285192 }");
        }});
        paramsMap.put("url5",new HashMap<String, String>(){{
            put("netid","test");
        }});
        paramsMap.put("url6",new HashMap<String, String>(){{
            put("parameters","{\"categories\":[\"Other\",\"Academic\"],\"keyword\":\"t\",\"searching\":true,\"startDate\":1470325889952}");
        }});
        paramsMap.put("url7",new HashMap<String, String>(){{
            put("parameters","{\"eventId\":\"1532545637229-0003\",\"type\":\"event\", \"userid\":\"test\"}");
        }});
        paramsMap.put("url8",new HashMap<String, String>(){{
            put("parameters","{\"device\":\"testDeviceID\",\"email\":\"test@test.com\",\"expiretime\":1469809961702,\"platform\":\"Android\",\"token\":\"fadGjkjgh4676HHFnsnf\",\"netid\":\"tester\"}");
        }});
        paramsMap.put("url9",new HashMap<String, String>(){{
            put("parameters","[{\"createTime\":1471119417199,\"data\":\"test\",\"description\":\"\",\"device\":\"Test Device ID\",\"event\":\"event name\",\"id\":0,\"lat\":40.9383469,\"lng\":-73.0862273,\"platform\":\"IOS\"}]");
        }});
        paramsMap.put("url11",new HashMap<String, String>(){{
            put("place","Syracuse University, Syracuse, NY, 13244");
        }});
        paramsMap.put("url12",new HashMap<String, String>(){{
            put("parameters","{ \"feeling\": \"Great\", \"description\": \"test description\", \"place\": \"Syracuse, NY 13210, USA\", \"lat\": 43.0373718, \"lng\": -76.1355275, \"createTime\": 1534300814, \"netid\": \"test\" }");
            put("filepath",System.getProperty("user.dir")+"/test.jpg");
        }});
        paramsMap.put("url13",new HashMap<String, String>(){{
            put("params","{ \"code\": 0, \"roomId\": \"FL001\", \"enterTime\": 1534300814, \"duration\": 1800000, \"lng\": -76.1355275, \"lat\": 43.0373718, \"by\": \"TEST\", \"locationSource\": \"TEST\", \"altit\": 0 }");
        }});
        paramsMap.put("url14",new HashMap<String, String>(){{
            put("parameters","{ \"enterTime\":1497997407486, \"netId\":\"test\", \"roomId\":\"FL001\", \"hidden\":0}");
        }});
        paramsMap.put("url15",new HashMap<String, String>(){{
            put("parameters","{ \"exitTime\":1534310814, \"roomlogId\":4 }");
        }});
        paramsMap.put("url16",new HashMap<String, String>(){{
            put("netId","test");
        }});
        paramsMap.put("url17",new HashMap<String, String>(){{
            put("day","5");
            put("roomId","FL001");
        }});
        paramsMap.put("url18",new HashMap<String, String>(){{
            put("beaconNumber","74278BDA-B644-4520-8F0C-720EAF059935-1-3");
        }});
        paramsMap.put("url19",new HashMap<String, String>(){{
            put("roomId","FL001");
        }});
        paramsMap.put("url22",new HashMap<String, String>(){{
            put("netId","test");
        }});
        paramsMap.put("url23",new HashMap<String, String>(){{
            put("parameters","{\"lat\":43.0391534,\"lng\":-76.1373045,\"query\":\"auditorium\"}");
        }});
        paramsMap.put("url25",new HashMap<String, String>(){{
            put("parameters","{\"age\":20,\"description\":\"I am description\",\"gender\":\"male\",\"hometown\":\"syracuse\",\"id\":0,\"intentions\":\"I am an intention\",\"major\":\"Computer Science\",\"netId\":\"test\",\"nickname\":\"test\",\"summary\":\"I am summary\",optout:0}");
        }});
        paramsMap.put("url26",new HashMap<String, String>(){{
            put("parameters","{\"categories\":[{\"category\":\"Academic\",\"isSelected\":true,\"selected\":true},{\"category\":\"Other\",\"isSelected\":true,\"selected\":true},{\"category\":\"Social\",\"isSelected\":true,\"selected\":true},{\"category\":\"Health\",\"isSelected\":true,\"selected\":true},{\"category\":\"Entertainment\",\"isSelected\":true,\"selected\":true},{\"category\":\"Work\",\"isSelected\":true,\"selected\":true}],\"netid\":\"test\"}");
        }});
        paramsMap.put("url27",new HashMap<String, String>(){{
            put("netId","test");
        }});
        paramsMap.put("url28",new HashMap<String, String>(){{
            put("netId","test");
            put("filepath",System.getProperty("user.dir")+"/test.jpg");
        }});

    }
}
